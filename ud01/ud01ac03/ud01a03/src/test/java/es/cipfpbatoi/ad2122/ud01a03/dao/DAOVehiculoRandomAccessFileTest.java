package es.cipfpbatoi.ad2122.ud01a03.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import es.cipfpbatoi.ad2122.ud01a03.model.Vehiculo;

@TestMethodOrder(OrderAnnotation.class)
class DAOVehiculoRandomAccessFileTest {
	
	private final static String FICHERO_GRUPO_1 = "src/test/resources/vehiculos-grupo-1.dat";
	private final static String FICHERO_GRUPO_2 = "src/test/resources/vehiculos-grupo-2.dat";
	private final static String FICHERO_GRUPO_3 = "src/test/resources/vehiculos-grupo-3.dat";

	@BeforeAll
	static void setUp() {
		File ficheroGrupo1 = new File(DAOVehiculoRandomAccessFileTest.FICHERO_GRUPO_1);
		if(ficheroGrupo1.exists()) {
			ficheroGrupo1.delete();
		}
		
		File ficheroGrupo2 = new File(DAOVehiculoRandomAccessFileTest.FICHERO_GRUPO_2);
		if(ficheroGrupo2.exists()) {
			ficheroGrupo2.delete();
		}

		File ficheroGrupo3 = new File(DAOVehiculoRandomAccessFileTest.FICHERO_GRUPO_3);
		if(ficheroGrupo3.exists()) {
			ficheroGrupo3.delete();
		}

		
		// RandomAccessFile espera que el fichero exista.
		try {
			ficheroGrupo1.createNewFile();
			ficheroGrupo2.createNewFile();
			ficheroGrupo3.createNewFile();
		} catch (IOException e) {
			fail();
		}
	}


	@Test
	@Order(1)
	void testPersistirUnVehiculo() {
		
		Vehiculo vehiculo = new Vehiculo();
    	vehiculo.setIdVehiculo(1);
    	vehiculo.setMarca("Citroen");
    	vehiculo.setModelo("C4");
    	vehiculo.setVersion("Cool");
    	vehiculo.setCilindrada(1500);
    	vehiculo.setPotencia(92.0);



    	
		DAOVehiculoRandomAccessFile daoVehiculo;
		try {
			daoVehiculo = new DAOVehiculoRandomAccessFile(DAOVehiculoRandomAccessFileTest.FICHERO_GRUPO_1);
			daoVehiculo.save(vehiculo);


		} catch (IOException e) {
			fail("El test debería haber almacenado el vehiculo");
		}
	}
	
	@Test
	@Order(2)
	void testLeerUnVehiculo() {
		Vehiculo vehiculo = null;
		
		DAOVehiculoRandomAccessFile daoVehiculo=null;
		try {
			daoVehiculo = new DAOVehiculoRandomAccessFile(DAOVehiculoRandomAccessFileTest.FICHERO_GRUPO_1);
			vehiculo = daoVehiculo.findById(1);
		} catch (FileNotFoundException e) {
			fail("El test debería haber leido el vehiculo");
		} catch (IOException e) {
			fail("El test debería haber leido el vehiculo");
		}
    	
    	
		assertEquals(vehiculo.getIdVehiculo(), 1);
		assertEquals(vehiculo.getMarca().toString().trim(), "Citroen");
		assertEquals(vehiculo.getModelo().toString().trim(), "C4");
		assertEquals(vehiculo.getVersion().toString().trim(), "Cool");
		assertEquals(vehiculo.getCilindrada(), 1500);
		assertEquals(vehiculo.getPotencia(), 92.0);
		
		try {
			vehiculo = daoVehiculo.findById(2);
		} catch (FileNotFoundException e) {
			fail("El test debería haber leido el vehiculo");
		} catch (IOException e) {
			fail("El test debería haber leido el vehiculo");
		}
		
		assertNull(vehiculo);
	}

	@Test
	@Order(3)
	void testPersistirUnVehiculo2() throws IOException {

		Vehiculo vehiculo = new Vehiculo();
		vehiculo.setIdVehiculo(1);
		vehiculo.setMarca("Citroen");
		vehiculo.setModelo("C4");
		vehiculo.setVersion("Cool");
		vehiculo.setCilindrada(1500);
		vehiculo.setPotencia(92.0);

		DAOVehiculoRandomAccessFile daoVehiculo = new DAOVehiculoRandomAccessFile(DAOVehiculoRandomAccessFileTest.FICHERO_GRUPO_3);


		Vehiculo vehiculo2 = new Vehiculo();
		vehiculo2.setIdVehiculo(2);
		vehiculo2.setMarca("Citroen");
		vehiculo2.setModelo("C3");
		vehiculo2.setVersion("Cool");
		vehiculo2.setCilindrada(1500);
		vehiculo2.setPotencia(92.0);


		daoVehiculo.save(vehiculo);
		daoVehiculo.save(vehiculo2);



	}

	@Test
	@Order(4)
	void findByMarca() throws FileNotFoundException, IOException {
		ArrayList<Vehiculo> vehiculos = new ArrayList<>();
		DAOVehiculoRandomAccessFile daoVehiculo;
		try {
			daoVehiculo = new DAOVehiculoRandomAccessFile(DAOVehiculoRandomAccessFileTest.FICHERO_GRUPO_3);
			vehiculos = daoVehiculo.findByMarca("Citroen");
		} catch (FileNotFoundException e) {
			fail("El test debería haber leido el vehiculo");
		} catch (IOException e) {
			fail("El test debería haber leido el vehiculo");
		}
		assertEquals(vehiculos.size(), 2);
	}
}
