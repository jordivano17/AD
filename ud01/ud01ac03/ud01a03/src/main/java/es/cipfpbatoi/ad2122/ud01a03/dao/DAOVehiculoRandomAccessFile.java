package es.cipfpbatoi.ad2122.ud01a03.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import es.cipfpbatoi.ad2122.ud01a03.model.Vehiculo;


public class DAOVehiculoRandomAccessFile {
	
	private File file ;

	public DAOVehiculoRandomAccessFile(String path) throws FileNotFoundException {
		this.file = new File(path);
	}
	
	public void save(Vehiculo vehiculo) throws IOException {
		
		RandomAccessFile randomAccessFile = null; 
		
		try {
			randomAccessFile = new RandomAccessFile(this.file, "rw");

			randomAccessFile.seek(file.length());
			randomAccessFile.writeInt(vehiculo.getIdVehiculo());
			randomAccessFile.writeChars(vehiculo.getMarca().toString());
			randomAccessFile.writeChars(vehiculo.getModelo().toString());
			randomAccessFile.writeChars(vehiculo.getVersion().toString());
			randomAccessFile.writeLong(vehiculo.getCilindrada());
			randomAccessFile.writeDouble(vehiculo.getPotencia());
		}
		finally {
			randomAccessFile.close();
		}
	}

	public Vehiculo findById(int idVehiculo) throws IOException {

		RandomAccessFile randomAccessFile = null; 
		try {
			randomAccessFile = new RandomAccessFile(file, "r"); 
			
			
			while (randomAccessFile.getFilePointer() < randomAccessFile.length() ) {
				
				Vehiculo vehiculo = new Vehiculo();
				
				vehiculo.setIdVehiculo(randomAccessFile.readInt());
				
				// Si el identificador del vehiculo no se corresponde con el del vehiculo que buscamos
				// saltamos al siguiente registro, sin leer todos los datos del vehiculo
				if(vehiculo.getIdVehiculo() != idVehiculo) {
					randomAccessFile.skipBytes(Vehiculo.BYTES_LENGHT_VEHICULO - 4);
					continue;
				}
				
				String marca = "";
				for (int i = 0; i < Vehiculo.CHAR_LENGHT_MARCA; i++) {
					marca = marca + randomAccessFile.readChar();
		        }
				vehiculo.setMarca(marca);
				
				String modelo = "";
				for (int i = 0; i < Vehiculo.CHAR_LENGHT_MODELO; i++) {
					modelo = modelo + randomAccessFile.readChar();
		        }
				vehiculo.setModelo(modelo);
				
				String version = "";
				for (int i = 0; i < Vehiculo.CHAR_LENGHT_VERSION; i++) {
					version = version + randomAccessFile.readChar();
		        }
				vehiculo.setVersion(version);
				
				vehiculo.setCilindrada(randomAccessFile.readLong());
				vehiculo.setPotencia(randomAccessFile.readDouble());
				
				// Como hemos encontrado el vehiculo con el identificador buscado,
				// y hemos leido todos sus datos, lo devolvemos.
				return vehiculo;
			}
			
			// Si hemos recorrido todo el fichero y no hemos encontrado el vehiculo,
			// devolvemos null
			return null;
			
		}finally {
			randomAccessFile.close();
		}
	}

	public ArrayList<Vehiculo> findByMarca(String Marca) throws IOException {
		ArrayList<Vehiculo> vehiculos = new ArrayList<>();
		try (
				RandomAccessFile randomAccessFile = new RandomAccessFile(this.file, "r");
		) {
			while (randomAccessFile.getFilePointer() < randomAccessFile.length()) {

				Vehiculo vehiculo = new Vehiculo();

				vehiculo.setIdVehiculo(randomAccessFile.readInt());
				// Si el identificador del vehiculo no se corresponde con el del vehiculo que buscamos
				// saltamos al siguiente registro, sin leer todos los datos del vehiculo

				String marcaEncontrada = "";
				for (int i = 0; i < Vehiculo.CHAR_LENGHT_MARCA; i++) {
					marcaEncontrada = marcaEncontrada + randomAccessFile.readChar();
				}
				vehiculo.setMarca(marcaEncontrada);
				if (!vehiculo.getMarca().toString().trim().equals(Marca)) {
					randomAccessFile.skipBytes(Vehiculo.BYTES_LENGHT_VEHICULO - 54);
					continue;
				}
				String modelo = "";
				for (int i = 0; i < Vehiculo.CHAR_LENGHT_MODELO; i++) {
					modelo = modelo + randomAccessFile.readChar();
				}
				vehiculo.setModelo(modelo);

				String version = "";
				for (int i = 0; i < Vehiculo.CHAR_LENGHT_VERSION; i++) {
					version = version + randomAccessFile.readChar();
				}
				vehiculo.setVersion(version);

				vehiculo.setCilindrada(randomAccessFile.readLong());
				vehiculo.setPotencia(randomAccessFile.readDouble());

				// Como hemos encontrado el vehiculo con el identificador buscado,
				// y hemos leido todos sus datos, lo devolvemos.
				vehiculos.add(vehiculo);



			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return vehiculos;
	}

}
