//Jordi Vañó Enrique
//Debes realizar un programa en java que se ejecutará vía consola y cuyo funcionamiento será el siguiente:

//        El programa permitirá mostrar los archivos de la carpeta actual filtrando sólo aquellos cuyo nombre contenga la
//        cadena que el usuario le pase como parámetro al programa (argumentos de entrada). Los archivos ocultos quedan descartados.
//        Uso del programa: $ java -jar lista <nom_buscar>
//        Si no recibe ningún parámetro, listará todos los archivos y carpetas.
//        [Opcional] El programa aceptará un segundo parámetro que será el nombre de una carpeta. Esta carpeta será el
//        destino de copia de todos los archivos que se encuentren en la búsqueda anterior (excepto carpetas).
//        Si la carpeta indicada no existe, deberá crearse.

//        Requisitos:
//        El programa deberá usar la interfaz FileFilter.
//        Pega la url del repositorio que contenga el código de tu práctica en Gitlab.
//        Para la parte opcional debes usar las clases de flujos de entrada/salida sobre ficheros (FileInputStream /FileOutputStream)

package es.cipfpbatoi.ad;

import java.io.File;
import java.util.Scanner;

public class Archivoapp1 {
    public static void main(String[] args) {
        File fichero=new File(".");
        String[] listaArchivos;

        if(args.length==0){
            listaArchivos= fichero.list();
            System.out.println(" ******* lista de los archivos de este directorio *******");
            for (int i = 0; i < listaArchivos.length; i++) {
                System.out.println(listaArchivos[i]);
            }
        }else {
            String Busqueda = args[0];
            listaArchivos= fichero.list(new Filtro(Busqueda));
            System.out.println(" ******* lista de los archivos con filtro *******");
            for(int i=0; i<listaArchivos.length; i++){
                System.out.println(listaArchivos[i]);
            }
        }
        }
    }