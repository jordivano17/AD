package es.cipfpbatoi.ad2122.ud01a02.dao;

import java.io.*;
import java.util.ArrayList;

import es.cipfpbatoi.ad2122.ud01a02.model.Vehiculo;

public class DAOOutputStreamVehiculo {
	
	File ficheroVehiculos;

	public DAOOutputStreamVehiculo(String fileName) {
		ficheroVehiculos = new File(fileName);
	}
	
	public void save(Vehiculo vehiculo) throws IOException {
		
		DataOutputStream dataOutputStream;
		dataOutputStream = new DataOutputStream(new FileOutputStream(ficheroVehiculos, true));
		
		dataOutputStream.writeInt(vehiculo.getIdVehiculo());
		dataOutputStream.writeUTF(vehiculo.getMarca());
		dataOutputStream.writeUTF(vehiculo.getModelo());
		dataOutputStream.writeUTF(vehiculo.getVersion());
		dataOutputStream.writeLong(vehiculo.getCilindrada());
		dataOutputStream.writeDouble(vehiculo.getPotencia());
			
		dataOutputStream.close();
	}

	public Vehiculo findById(int idVehiculo) throws IOException {
		
		FileInputStream fileInputStream = new FileInputStream(this.ficheroVehiculos);
		DataInputStream dataInputStream = new DataInputStream(fileInputStream);
		
		Vehiculo vehiculo = null;
		while (dataInputStream.available() > 0) {
			vehiculo = this.readItem(dataInputStream);
			if (vehiculo.getIdVehiculo() == idVehiculo)
				break;
			
			vehiculo = null;
		}
		
		fileInputStream.close();
		dataInputStream.close();
		
		return vehiculo;
	}

	private Vehiculo readItem(DataInputStream dataInputStream) throws IOException {
		Vehiculo vehiculo = new Vehiculo();
		
		vehiculo.setIdVehiculo(dataInputStream.readInt());
		vehiculo.setMarca(dataInputStream.readUTF());
		vehiculo.setModelo(dataInputStream.readUTF());
		vehiculo.setVersion(dataInputStream.readUTF());
		vehiculo.setCilindrada(dataInputStream.readLong());
		vehiculo.setPotencia(dataInputStream.readDouble());
		
		return vehiculo;
	}


	public ArrayList<Vehiculo> findAll(String marca) throws IOException {

		Vehiculo vehiculo = null;
		ArrayList<Vehiculo> listado = new ArrayList<>();
		InputStream is = null;
		DataInputStream dis = null;

			try{
				is = new FileInputStream(this.ficheroVehiculos);
				dis = new DataInputStream(is);

				while(dis.available()>0){
					vehiculo = this.readItem(dis);
					if(vehiculo.getMarca().equals(marca)){
						listado.add(vehiculo);
					}
				}
			}
			finally {
				is.close();
				dis.close();
			}
		return listado;
	}

}
