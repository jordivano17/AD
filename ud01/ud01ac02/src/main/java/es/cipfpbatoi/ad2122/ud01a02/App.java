package es.cipfpbatoi.ad2122.ud01a02;

import java.io.IOException;

import es.cipfpbatoi.ad2122.ud01a02.dao.DAOOutputStreamVehiculo;
import es.cipfpbatoi.ad2122.ud01a02.model.Vehiculo;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        
    	DAOOutputStreamVehiculo daoVehiculo = new DAOOutputStreamVehiculo("prueba.dat");
    	
    	Vehiculo vehiculo1 = new Vehiculo();
    	vehiculo1.setIdVehiculo(1);
    	vehiculo1.setMarca("Citroen");
    	vehiculo1.setModelo("C4");
    	vehiculo1.setVersion("Cool");
    	vehiculo1.setCilindrada(1500);
    	vehiculo1.setPotencia(92.0);

		Vehiculo vehiculo2 = new Vehiculo();
		vehiculo2.setIdVehiculo(2);
		vehiculo2.setMarca("Seat");
		vehiculo2.setModelo("Ibiza");
		vehiculo2.setVersion("Style");
		vehiculo2.setCilindrada(1000);
		vehiculo2.setPotencia(80.0);

		Vehiculo vehiculo3 = new Vehiculo();
		vehiculo3.setIdVehiculo(3);
		vehiculo3.setMarca("Citroen");
		vehiculo3.setModelo("C3");
		vehiculo3.setVersion("Cool");
		vehiculo3.setCilindrada(1600);
		vehiculo3.setPotencia(100.0);

		Vehiculo vehiculo = new Vehiculo();
		vehiculo.setIdVehiculo(4);
		vehiculo.setMarca("Seat");
		vehiculo.setModelo("Leon");
		vehiculo.setVersion("FR");
		vehiculo.setCilindrada(2000);
		vehiculo.setPotencia(150.0);

    	try {
			daoVehiculo.save(vehiculo1);
			daoVehiculo.save(vehiculo2);
			daoVehiculo.save(vehiculo3);
			daoVehiculo.save(vehiculo);
		} catch (IOException e) {
			System.err.println("Error al almacenar vehiculo.");
			e.printStackTrace();
			return;
		}
    	
    	Vehiculo vehiculoLeido;
		try {
			vehiculoLeido = daoVehiculo.findById(1);
			vehiculoLeido = daoVehiculo.findById(2);
			vehiculoLeido = daoVehiculo.findById(3);
			vehiculoLeido = daoVehiculo.findById(4);
		} catch (IOException e) {
			System.err.println("Error al recuperar vehiculo.");
			e.printStackTrace();
			return;
		}
		
    	System.out.println(vehiculoLeido.prettyPrint());
    	
		
    }
}
