//A partir del código creado en clase, utilizado para almacenar y recuperar datos de vehículos:

//https://gitlab.com/2122-acceso-a-datos/profesor/ejemplos/ud01/-/tree/main/ud01a02

//Añadir la siguiente funcionalidad:

//Crear un método en la clase DAOOutputStreamVehiculo llamado findAll(String marca) que devuelva todos los registros de vehículos de la marca que se pasa como parámetro.
//[Opcional] Implementa la clase DAOObjectOutputStreamVehiculo para que realice el mismo proceso utilizando las clases ObjectInputStream / ObjectOutputStream. Se debe evitar el problema de la cabecera comentado en clase.
//Posible solución a este problema: http://www.chuidiang.org/java/ficheros/ObjetosFichero.php
//Entrega:

//Opción preferente: pega el link de gitlab donde has almacenado el proyecto.
//Alternativa: pega el método o métodos que hayas creado en el cuadro de texto de la entrega con estilo preformateado.

package es.cipfpbatoi.ad2122.ud01a02;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.*;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import es.cipfpbatoi.ad2122.ud01a02.dao.DAOOutputStreamVehiculo;
import es.cipfpbatoi.ad2122.ud01a02.model.Vehiculo;

@TestMethodOrder(OrderAnnotation.class)
class DAOOutputStreamVehiculoTest {

	private final static String FICHERO_GRUPO_1 = "src/test/resources/vehiculos-grupo-1.dat";
	private final static String FICHERO_GRUPO_2 = "src/test/resources/vehiculos-grupo-2.dat";
	private final static String FICHERO_GRUPO_3 = "src/test/resources/vehiculos-varias-sesiones.dat";

	@BeforeAll
	static void setUp() {
		File ficheroGrupo1 = new File(DAOOutputStreamVehiculoTest.FICHERO_GRUPO_1);
		if(ficheroGrupo1.exists()) {
			ficheroGrupo1.delete();
		}

		File ficheroGrupo2 = new File(DAOOutputStreamVehiculoTest.FICHERO_GRUPO_2);
		if(ficheroGrupo2.exists()) {
			ficheroGrupo2.delete();
		}

		// RandomAccessFile espera que el fichero exista.
		try {
			ficheroGrupo1.createNewFile();
			ficheroGrupo2.createNewFile();
		} catch (IOException e) {
			fail();
		}
	}



	@Test
	@Order(1)
	void testPersistirUnVehiculo() {
		DAOOutputStreamVehiculo daoVehiculo = new DAOOutputStreamVehiculo(DAOOutputStreamVehiculoTest.FICHERO_GRUPO_1);
    	
    	Vehiculo vehiculo = new Vehiculo();
    	vehiculo.setIdVehiculo(1);
    	vehiculo.setMarca("Citroen");
    	vehiculo.setModelo("C4");
    	vehiculo.setVersion("Cool");
    	vehiculo.setCilindrada(1500);
    	vehiculo.setPotencia(92.0);

    	try {
			daoVehiculo.save(vehiculo);
		} catch (IOException e) {
			fail("El test debería haber almacenado el vehiculo");
		}
	}
	
	@Test
	@Order(2)
	void testLeerUnVehiculo() {
		DAOOutputStreamVehiculo daoVehiculo = new DAOOutputStreamVehiculo(DAOOutputStreamVehiculoTest.FICHERO_GRUPO_1);
    	
    	Vehiculo vehiculo = null;
		try {
			vehiculo = daoVehiculo.findById(1);
		} catch (IOException e1) {
			fail("El test debería haber leido el vehiculo");
		}
		
		assertEquals(vehiculo.getIdVehiculo(), 1);
		assertEquals(vehiculo.getMarca(), "Citroen");
		assertEquals(vehiculo.getModelo(), "C4");
		assertEquals(vehiculo.getVersion(), "Cool");
		assertEquals(vehiculo.getCilindrada(), 1500);
		assertEquals(vehiculo.getPotencia(), 92.0);
	}
	
	@Test
	@Order(3)
	void testPersistirVariosVehiculos() {
		DAOOutputStreamVehiculo daoVehiculo = new DAOOutputStreamVehiculo(DAOOutputStreamVehiculoTest.FICHERO_GRUPO_2);
    	
    	Vehiculo vehiculo = new Vehiculo();
    	vehiculo.setIdVehiculo(1);
    	vehiculo.setMarca("Citroen");
    	vehiculo.setModelo("C4");
    	vehiculo.setVersion("Cool");
    	vehiculo.setCilindrada(1500);
    	vehiculo.setPotencia(92.0);

    	try {
			daoVehiculo.save(vehiculo);
		} catch (IOException e) {
			fail("El test debería haber almacenado el vehiculo");
		}

		vehiculo = new Vehiculo();
    	vehiculo.setIdVehiculo(2);
    	vehiculo.setMarca("Seat");
    	vehiculo.setModelo("Leon");
    	vehiculo.setVersion("Sport");
    	vehiculo.setCilindrada(1900);
    	vehiculo.setPotencia(150.0);

    	try {
			daoVehiculo.save(vehiculo);
		} catch (IOException e) {
			fail("El test debería haber almacenado el vehiculo");
		}

		vehiculo = new Vehiculo();
		vehiculo.setIdVehiculo(3);
		vehiculo.setMarca("Seat");
		vehiculo.setModelo("Ibiza");
		vehiculo.setVersion("Sport");
		vehiculo.setCilindrada(1900);
		vehiculo.setPotencia(150.0);

		try {
			daoVehiculo.save(vehiculo);
		} catch (IOException e) {
			fail("El test debería haber almacenado el vehiculo");
		}
	}
	
	@Test
	@Order(4)
	void testLeerVariosVehiculo() {
		DAOOutputStreamVehiculo daoVehiculo = new DAOOutputStreamVehiculo(DAOOutputStreamVehiculoTest.FICHERO_GRUPO_2);
    	

		ArrayList<Vehiculo> lista = null;
		try{
			lista = daoVehiculo.findAll("Seat");
		}catch (IOException e1){
			fail("No se ha podido hacer un listado en el test.");
		}
		assertEquals(lista.size(), 2);

	}

	@Test
	@Order(5)
	void testBuscarPorMarca() throws IOException {
		long pos=0;
		StringBuilder auxBuilder;
		int cont;
		String cadena;
		Vehiculo vehiculo = null;
		try{
			RandomAccessFile fichero = new RandomAccessFile(DAOOutputStreamVehiculoTest.FICHERO_GRUPO_1, "rw");
			cadena=fichero.readLine();
			while(cadena!=null){
				cont=cadena.indexOf("Seat");
				while(cont!=-1) {
					auxBuilder = new StringBuilder(cadena);
					cadena = auxBuilder.toString();

					fichero.seek(pos);
					cont=cadena.indexOf("Seat");
				}
				pos=fichero.getFilePointer();
				cadena=fichero.readLine();
				}
			fichero.close();
		}catch (FileNotFoundException ex){
			System.out.println(ex.getMessage());
		}catch (IOException e2){
			fail("El test debería haber leído el vehículo");
		}
	}
}
