package es.cipfpbatoi.ad2122.ud01a03.dao;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import es.cipfpbatoi.ad2122.ud01a03.model.Vehiculo;
import es.cipfpbatoi.ad2122.ud01a03.model.Vehiculos;


public class DAOVehiculoXMLFile {

	private File file;

	public DAOVehiculoXMLFile(String path) {
		this.file = new File(path);
	}

	public void save(List<Vehiculo> listaVehiculos) throws JsonGenerationException, JsonMappingException, IOException{
		Vehiculos vehiculos = new Vehiculos();
		vehiculos.setVehiculos(listaVehiculos);

		XmlMapper xmlMapper = new XmlMapper();
		xmlMapper.writeValue(this.file, vehiculos);
	}

	public List<Vehiculo> findAll() throws JsonParseException, JsonMappingException, IOException {
		XmlMapper xmlMapper = new XmlMapper();
		Vehiculos vehiculos = xmlMapper.readValue(this.file, Vehiculos.class);
		return vehiculos.getVehiculos();
	}
}
