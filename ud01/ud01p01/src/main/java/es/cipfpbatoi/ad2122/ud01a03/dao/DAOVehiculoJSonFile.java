package es.cipfpbatoi.ad2122.ud01a03.dao;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.cipfpbatoi.ad2122.ud01a03.model.Vehiculo;
import es.cipfpbatoi.ad2122.ud01a03.model.Vehiculos;


public class DAOVehiculoJSonFile {
	
	private File file ;

	public DAOVehiculoJSonFile(String path) {
		this.file = new File(path);
	}
	
	public void save(List<Vehiculo> listaVehiculos) throws JsonGenerationException, JsonMappingException, IOException {
		Vehiculos vehiculos = new Vehiculos();
		vehiculos.setVehiculos(listaVehiculos);

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.writeValue(this.file,vehiculos);
	}

	public List<Vehiculo> findAll() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		Vehiculos vehiculos = objectMapper.readValue(this.file, Vehiculos.class);
		return vehiculos.getVehiculos();
	}

}
