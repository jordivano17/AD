package es.cipfpbatoi.ad2122.ud01a03.dao;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


import es.cipfpbatoi.ad2122.ud01a03.model.Vehiculo;



public class DAOVehiculoObjectIOStream {
	
	private File file ;

	public DAOVehiculoObjectIOStream(String path) throws FileNotFoundException {
		this.file = new File(path);
	}

	public void save(List<Vehiculo> vehiculos) throws IOException {
		OutputStream outputStream = null;
		ObjectOutputStream objectOutputStream = null;
		//Generamos tanto un OutputStream como un ObjectOutputStream para poder escribir los vehículos del arrayList vehiculos.
		try {
			outputStream = new FileOutputStream(this.file, true);//Le marcamos el archivo
			objectOutputStream = new ObjectOutputStream(outputStream);

			for(int i=0;i<vehiculos.size();i++){
				objectOutputStream.writeObject(vehiculos.get(i));
				//Escribimos mediante este for todos los vehiculos, uno a uno, en el archivo marcado anteriormente
			}

		} finally {
			objectOutputStream.flush();//"Publicamos" los objetos leídos
			objectOutputStream.close();
			outputStream.close();
			//Aplicando el .close cerramos los objectOutputStream y el outputStream para que finalice bien el proceso.
		}

	}
	public List<Vehiculo> findAll() throws IOException {
		List<Vehiculo> vehiculos = new ArrayList<>();
		Object object = null;//generamos un objeto vacío para poder sacar cada objeto del archivo conforme se vaya leyendo
		ObjectInputStream objectInputStream = null;
		//Generamos un ObjectInputStream para poder leer los vehículos del archivo.
		try {
			objectInputStream = new ObjectInputStream(new FileInputStream(this.file)); //Mediante el FileInputStrem hacemos que se puedan leer el archivo
			while((object=objectInputStream.readObject())!=null){
				Vehiculo vehiculo = (Vehiculo) object;
				vehiculos.add(vehiculo);
			}//Con este bucle comparo que conforme vaya leyendo objetos del archivo, se vayan generando en el objeto nulo creado anteriormente
			// dandole el valor al vehículo y que acto seguido se añada al ArrayList.
		}catch (IOException e){
			e.printStackTrace();
		} finally {
			return vehiculos; //devolvemos el arrayList de Vehículos
		}
	}
}
