package es.cipfpbatoi.ud02a01.model;

public class Grupos {

    private int id;
    private String descripcion;

    public Grupos() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
