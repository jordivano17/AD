package es.cipfpbatoi.ud02a01.repository.jdbc.cliente;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Repository;

import es.cipfpbatoi.ud02a01.model.Cliente;

@Repository
public class ClienteJDBCDAO {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	private static final String SELECT_COUNT =  "SELECT COUNT(*) FROM CLIENTES";
	
	private static final String SELECT_ALL =  "SELECT * FROM CLIENTES";
	private static final String SELECT_BY_ID =  "SELECT * FROM CLIENTES WHERE ID = ?";
	
	
	public int count() {
		return jdbcTemplate.queryForObject(ClienteJDBCDAO.SELECT_COUNT, Integer.class);
	}
	
	public List<Cliente> findAll() {
		
		List<Cliente> clientes = this.jdbcTemplate.query(
				new PreparedStatementCreator() {
					@Override
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				
						return connection.prepareStatement(SELECT_ALL, Statement.CLOSE_CURRENT_RESULT);
	            
					}
			
				}, 
				new ClientesRowMapper()
		);
		
		return clientes;
	}
	
	public Cliente findById(Integer id) {
		
		Cliente cliente = this.jdbcTemplate.query(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				
				PreparedStatement ps =   connection.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
	            ps.setInt(1, id);
	            return ps;
	            
			}
			
		}, new ClienteResultSetExtractor());
		
		return cliente;
	}
	
	public Cliente insert(Cliente cliente) {
		
		
		
		return null;
	}
	
	public Cliente update(Cliente cliente) {
		
		
		
		return null;
	}
	
	public Cliente delete(Cliente cliente) {
		
		
		
		return null;
	}
	
}
