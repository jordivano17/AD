package es.cipfpbatoi.ud02a01.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.repository.jdbc.cliente.ClienteJDBCDAO;

@RestController
@RequestMapping("/clientes")
public class ClientesController {
	
	@Autowired
	ClienteJDBCDAO clienteJDBCDAO;

	@GetMapping("/count")
	public Integer count() {
		return this.clienteJDBCDAO.count();
	}
	
	@GetMapping("/")
	public List<Cliente> findAll() {
		return this.clienteJDBCDAO.findAll();
	}
	
	@GetMapping("/{id}")
	public Cliente findById(@PathVariable int id) {
		return this.clienteJDBCDAO.findById(id);
	}

}
