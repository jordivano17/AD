package es.cipfpbatoi.ud02a01.controller;

import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.model.Vendedor;
import es.cipfpbatoi.ud02a01.repository.jdbc.vendedor.VendedorJDBCDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/vendedores")
public class VendedoresController {

    @Autowired
    VendedorJDBCDAO vendedorJDBCDAO;
    @GetMapping("/count")
    public Integer count(){
        return this.vendedorJDBCDAO.count();
    }

    @GetMapping("/")
    public List<Vendedor> findAll() {
        return this.vendedorJDBCDAO.findAll();
    }

    @GetMapping("/{id}")
    public Vendedor findById(@PathVariable int id) {return this.vendedorJDBCDAO.findById(id);
    }
}
