package es.cipfpbatoi.ud02a01.repository.jdbc.grupos;

import es.cipfpbatoi.ud02a01.model.Grupos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Repository
public class GruposJDBCDAO {
    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String SELECT_COUNT = "SELECT COUNT(*) FROM GRUPOS";

    private static final String SELECT_ALL =  "SELECT * FROM GRUPOS";
    private static final String SELECT_BY_ID =  "SELECT * FROM GRUPOS WHERE ID = ?";

    public int count(){return jdbcTemplate.queryForObject(GruposJDBCDAO.SELECT_COUNT, Integer.class);}

    public List<Grupos> findAll() {

        List<Grupos> grupos = this.jdbcTemplate.query(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        return connection.prepareStatement(SELECT_ALL, Statement.CLOSE_CURRENT_RESULT);

                    }

                },
                new GruposRowMapper()
        );

        return grupos;
    }

    public Grupos findById(Integer id) {

        Grupos grupos = this.jdbcTemplate.query(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                PreparedStatement ps =   connection.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
                ps.setInt(1, id);
                return ps;

            }

        }, new GruposResultSetExtractor());

        return grupos;
    }

    public Grupos insert(Grupos grupos) {



        return null;
    }

    public Grupos update(Grupos grupos) {



        return null;
    }

    public Grupos delete(Grupos grupos) {



        return null;
    }
}
