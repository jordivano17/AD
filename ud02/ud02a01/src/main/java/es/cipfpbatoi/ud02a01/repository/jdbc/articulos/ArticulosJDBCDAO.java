package es.cipfpbatoi.ud02a01.repository.jdbc.articulos;

import es.cipfpbatoi.ud02a01.model.Articulos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Repository
public class ArticulosJDBCDAO {
    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String SELECT_COUNT = "SELECT COUNT(*) FROM ARTICULOS";

    private static final String SELECT_ALL =  "SELECT * FROM ARTICULOS";
    private static final String SELECT_BY_ID =  "SELECT * FROM ARTICULOS WHERE ID = ?";

    public int count(){return jdbcTemplate.queryForObject(ArticulosJDBCDAO.SELECT_COUNT,Integer.class);}

    public List<Articulos> findAll() {

        List<Articulos> articulos = this.jdbcTemplate.query(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        return connection.prepareStatement(SELECT_ALL, Statement.CLOSE_CURRENT_RESULT);

                    }

                },
                new ArticulosRowMapper()
        );

        return articulos;
    }

    public Articulos findById(Integer id) {

        Articulos articulos = this.jdbcTemplate.query(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                PreparedStatement ps =   connection.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
                ps.setInt(1, id);
                return ps;

            }

        }, new ArticulosResultSetExtractor());

        return articulos;
    }

    public Articulos insert(Articulos articulos) {



        return null;
    }

    public Articulos update(Articulos articulos) {



        return null;
    }

    public Articulos delete(Articulos articulos) {



        return null;
    }
}
