package es.cipfpbatoi.ud02a01.repository.jdbc.vendedor;


import es.cipfpbatoi.ud02a01.model.Vendedor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Repository
    public class VendedorJDBCDAO{
        @Autowired
        JdbcTemplate jdbcTemplate;

        private static final String SELECT_COUNT = "SELECT COUNT(*) FROM VENDEDORES";

        private static final String SELECT_ALL =  "SELECT * FROM VENDEDORES";
        private static final String SELECT_BY_ID =  "SELECT * FROM VENDEDORES WHERE ID = ?";



        public int count(){
            return jdbcTemplate.queryForObject(VendedorJDBCDAO.SELECT_COUNT, Integer.class);
        }

    public List<Vendedor> findAll() {

        List<Vendedor> vendedores = this.jdbcTemplate.query(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        return connection.prepareStatement(SELECT_ALL, Statement.CLOSE_CURRENT_RESULT);

                    }

                },
                new VendedorRowMapper()
        );

        return vendedores;
    }

    public Vendedor findById(Integer id) {

        Vendedor vendedores = this.jdbcTemplate.query(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                PreparedStatement ps =   connection.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
                ps.setInt(1, id);
                return ps;

            }

        }, new VendedorResultSetExtractor());

        return vendedores;
    }

    public Vendedor insert(Vendedor vendedor) {



        return null;
    }

    public Vendedor update(Vendedor vendedor) {



        return null;
    }

    public Vendedor delete(Vendedor vendedor) {



        return null;
    }
}
