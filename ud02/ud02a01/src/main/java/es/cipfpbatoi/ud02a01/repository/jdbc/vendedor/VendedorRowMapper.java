package es.cipfpbatoi.ud02a01.repository.jdbc.vendedor;

import es.cipfpbatoi.ud02a01.model.Vendedor;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VendedorRowMapper implements RowMapper<Vendedor> {

    @Override
    public Vendedor mapRow(ResultSet rs, int rowNum) throws SQLException {

        Vendedor vendedor = new Vendedor();
        vendedor.setId(rs.getInt("id"));
        vendedor.setNombre(rs.getString("nombre"));
        vendedor.setFechaIngreso(rs.getDate("fecha_ingreso"));
        vendedor.setSalario(rs.getDouble("salario"));
        return vendedor;

    }
}
