package es.cipfpbatoi.ud02a01.repository.jdbc.cliente;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import es.cipfpbatoi.ud02a01.model.Cliente;

public class ClientesRowMapper implements RowMapper<Cliente>{

	@Override
	public Cliente mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Cliente cliente = new Cliente();
		cliente.setId(rs.getInt("id"));
		cliente.setNombre(rs.getString("nombre"));
		cliente.setDireccion(rs.getString("direccion"));
		return cliente;
		
	}

}