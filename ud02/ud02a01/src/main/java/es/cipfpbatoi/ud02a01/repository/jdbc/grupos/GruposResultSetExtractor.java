package es.cipfpbatoi.ud02a01.repository.jdbc.grupos;

import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.model.Grupos;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GruposResultSetExtractor implements ResultSetExtractor<Grupos> {

    @Override
    public Grupos extractData(ResultSet rs) throws SQLException, DataAccessException {

        if(!rs.next()) {
            return null;
        }

        Grupos grupos = new Grupos();

        grupos.setId(rs.getInt("id"));
        grupos.setDescripcion(rs.getString("descripcion"));

        return grupos;
    }

}
