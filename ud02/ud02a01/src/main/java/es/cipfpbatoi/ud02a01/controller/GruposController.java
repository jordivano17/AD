package es.cipfpbatoi.ud02a01.controller;


import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.model.Grupos;
import es.cipfpbatoi.ud02a01.repository.jdbc.grupos.GruposJDBCDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/grupos")
public class GruposController {
    @Autowired
    GruposJDBCDAO gruposJDBCDAO;
    @GetMapping("/count")
    public Integer count(){return this.gruposJDBCDAO.count();}



    @GetMapping("/")
    public List<Grupos> findAll() {
        return this.gruposJDBCDAO.findAll();
    }

    @GetMapping("/{id}")
    public Grupos findById(@PathVariable int id) {
        return this.gruposJDBCDAO.findById(id);
    }
}
