package es.cipfpbatoi.ud02a01.repository.jdbc.articulos;

import es.cipfpbatoi.ud02a01.model.Articulos;
import es.cipfpbatoi.ud02a01.model.Cliente;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ArticulosResultSetExtractor implements ResultSetExtractor<Articulos> {

    @Override
    public Articulos extractData(ResultSet rs) throws SQLException, DataAccessException {

        if(!rs.next()) {
            return null;
        }

        Articulos articulos = new Articulos();

        articulos.setId(rs.getInt("id"));
        articulos.setNombre(rs.getString("nombre"));
        articulos.setCodigo(rs.getString("codigo"));
        articulos.setGrupo(rs.getInt("grupo"));
        articulos.setPrecio(rs.getDouble("precio"));

        return articulos;
    }
}
