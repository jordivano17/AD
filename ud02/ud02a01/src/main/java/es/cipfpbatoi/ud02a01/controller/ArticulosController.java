package es.cipfpbatoi.ud02a01.controller;


import es.cipfpbatoi.ud02a01.model.Articulos;
import es.cipfpbatoi.ud02a01.repository.jdbc.articulos.ArticulosJDBCDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/articulos")
public class ArticulosController {
    @Autowired
    ArticulosJDBCDAO articulosJDBCDAO;
    @GetMapping("/count")
    public Integer count(){return this.articulosJDBCDAO.count();}

    @GetMapping("/")
    public List<Articulos> findAll() {
        return this.articulosJDBCDAO.findAll();
    }

    @GetMapping("/{id}")
    public Articulos findById(@PathVariable int id) {
        return this.articulosJDBCDAO.findById(id);
    }
}
