package es.cipfpbatoi.ud02a01.controller;


import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.model.Grupos;
import es.cipfpbatoi.ud02a01.repository.csv.cliente.ClienteCsvDAO;
import es.cipfpbatoi.ud02a01.repository.csv.grupos.GrupoCsvDAO;
import es.cipfpbatoi.ud02a01.repository.jdbc.grupos.GruposJDBCDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping("/grupos")
public class GruposController {
    @Autowired
    GruposJDBCDAO gruposJDBCDAO;

    @Autowired
    GrupoCsvDAO grupoCsvDAO;

    @PostMapping("")
    @ResponseStatus(code= HttpStatus.CREATED)
    public Grupos create(@RequestBody Grupos grupos) {

        Grupos grupInsertado = this.gruposJDBCDAO.insert(grupos);
        if(grupInsertado == null) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not created.");
        }

        return grupInsertado;
    }

    @GetMapping("/count")
    public Integer count() {
        return this.gruposJDBCDAO.count();
    }

    @GetMapping("")
    public List<Grupos> findAll() {
        return this.gruposJDBCDAO.findAll();
    }

    @GetMapping(value="", params = "fileFormat")
    public ResponseEntity<Resource> findAll(@RequestParam String fileFormat) {
        List<Grupos> grupos = this.gruposJDBCDAO.findAll();
        File file = null;




        switch (fileFormat) {
            case "csv":
                file = this.grupoCsvDAO.writeCSV(grupos);
                break;

            default:
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "fileFormat not supported.");
        }

        if(file == null) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "File not generated.");
        }

        Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource;
        try {
            resource = new ByteArrayResource(Files.readAllBytes(path));

            return ResponseEntity.ok()
                    .header("Content-disposition", "attachment; filename="+ file.getName())
                    .header("Cache-Control", "no-cache, no-store, must-revalidate")
                    .header("Pragma", "no-cache")
                    .header("Expires", "0")
                    .contentLength(file.length())
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        }
        catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "File cannot be downloaded.");
        }
        finally {
            file.delete();
        }
    }

    @GetMapping("/{id}")
    public Grupos findById(@PathVariable int id) {
        Grupos grupos = this.gruposJDBCDAO.findById(id);
        if(grupos == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Entity not found.");
        }
        return grupos;
    }

    @PutMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity updated" )
    public void update(@PathVariable int id, @RequestBody Grupos grupos) {

        boolean updated = this.gruposJDBCDAO.update(grupos);
        if(!updated) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not updated.");
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity deleted" )
    public void delete(@PathVariable int id, @RequestBody Grupos grupos) {

        boolean deleted = this.gruposJDBCDAO.delete(grupos);
        if(!deleted) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not deleted.");
        }
    }
}
