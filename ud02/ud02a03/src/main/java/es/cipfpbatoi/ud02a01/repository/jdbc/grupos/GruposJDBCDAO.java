package es.cipfpbatoi.ud02a01.repository.jdbc.grupos;

import es.cipfpbatoi.ud02a01.model.Grupos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Repository
public class GruposJDBCDAO {
    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String SELECT_COUNT = "SELECT COUNT(*) FROM GRUPOS";

    private static final String SELECT_ALL =  "SELECT * FROM GRUPOS";
    private static final String SELECT_BY_ID =  "SELECT * FROM GRUPOS WHERE ID = ?";

    private static final String INSERT =  "INSERT INTO GRUPOS(id, direccion) VALUES(?,?)";
    private static final String UPDATE =  "UPDATE GRUPOS SET id=?, direccion=? WHERE id=?";
    private static final String DELETE =  "DELETE GRUPOS WHERE id=?";

    public int count(){return jdbcTemplate.queryForObject(GruposJDBCDAO.SELECT_COUNT, Integer.class);}

    public List<Grupos> findAll() {

        List<Grupos> grupos = this.jdbcTemplate.query(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        return connection.prepareStatement(SELECT_ALL, Statement.CLOSE_CURRENT_RESULT);

                    }

                },
                new GruposRowMapper()
        );

        return grupos;
    }

    public Grupos findById(Integer id) {

        Grupos grupos = this.jdbcTemplate.query(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps =   connection.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
                ps.setInt(1, id);
                return ps;
            }
        }, new GruposResultSetExtractor());
        return grupos;
    }

    public Grupos insert(Grupos grupos) {

        KeyHolder keyHolder = new GeneratedKeyHolder();

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =  connection.prepareStatement(
                                INSERT,
                                Statement.CLOSE_CURRENT_RESULT);

                        ps.setString(1, grupos.getDescripcion());
                        return ps;
                    }
                }, keyHolder);

        if(resultadoOperacion != 1) {
            return null;
        }

        grupos.setId(keyHolder.getKey().intValue());
        return grupos;
    }

    public Boolean update(Grupos grupos) {

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(
                                UPDATE,
                                Statement.CLOSE_CURRENT_RESULT);

                        ps.setString(1, grupos.getDescripcion());
                        ps.setInt(2, grupos.getId());
                        return ps;
                    }
                });

        return resultadoOperacion == 1;
    }

    public Boolean delete(Grupos grupos) {

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(
                                DELETE,
                                Statement.CLOSE_CURRENT_RESULT);
                        ps.setInt(1, grupos.getId());
                        return ps;
                    }
                });

        return resultadoOperacion == 1;
    }
}
