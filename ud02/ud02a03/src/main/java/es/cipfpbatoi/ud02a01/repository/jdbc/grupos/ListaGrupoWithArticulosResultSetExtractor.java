package es.cipfpbatoi.ud02a01.repository.jdbc.grupos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import es.cipfpbatoi.ud02a01.model.Articulos;
import es.cipfpbatoi.ud02a01.model.Grupos;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;



public class ListaGrupoWithArticulosResultSetExtractor implements ResultSetExtractor<List<Grupos>> {

    private List<Grupos> grupos = new ArrayList<>();

    @Override
    public List<Grupos> extractData(ResultSet rs) throws SQLException, DataAccessException {

        while(rs.next()) {

            Integer idGrupo = rs.getInt("grupos.id");

            Grupos grupo = grupos.stream()
                    .filter(grupoActual -> idGrupo == grupoActual.getId())
                    .findAny()
                    .orElse(null);

            //Grupo grupo = null;
            //for(Grupo grupoActual: this.grupos) {
            //	if(idGrupo == grupoActual.getId()) {
            //		grupo = grupoActual;
            //		break;
            //	}
            //}

            if(grupo == null) {
                grupo = new Grupos();
                this.grupos.add(grupo);

                grupo.setId(idGrupo);
                grupo.setDescripcion(rs.getString("grupos.descripcion"));
            }

            Articulos articulo = new Articulos();
            grupo.addArticulo(articulo);

            articulo.setId(rs.getInt("articulos.id"));
            articulo.setIdGrupo(rs.getInt("grupos.id"));
            articulo.setCodigo(rs.getString("articulos.codigo"));
            articulo.setNombre(rs.getString("articulos.nombre"));
            articulo.setPrecio(rs.getDouble("articulos.precio"));

        };

        return this.grupos;
    }
}

