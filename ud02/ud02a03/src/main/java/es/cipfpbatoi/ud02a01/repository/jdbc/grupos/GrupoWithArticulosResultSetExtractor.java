package es.cipfpbatoi.ud02a01.repository.jdbc.grupos;

import java.sql.ResultSet;
import java.sql.SQLException;

import es.cipfpbatoi.ud02a01.model.Articulos;
import es.cipfpbatoi.ud02a01.model.Grupos;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

public class GrupoWithArticulosResultSetExtractor implements ResultSetExtractor<Grupos> {

    @Override
    public Grupos extractData(ResultSet rs) throws SQLException, DataAccessException {

        if(!rs.next()) {
            return null;
        }

        Grupos grupo = new Grupos();
        grupo.setId(rs.getInt("grupos.id"));
        grupo.setDescripcion(rs.getString("grupos.descripcion"));

        do {
            Articulos articulo = new Articulos();
            articulo.setGrupo(null);

            grupo.addArticulo(articulo);

            articulo.setId(rs.getInt("articulos.id"));
            articulo.setIdGrupo(rs.getInt("grupos.id"));
            articulo.setCodigo(rs.getString("articulos.codigo"));
            articulo.setNombre(rs.getString("articulos.nombre"));
            articulo.setPrecio(rs.getDouble("articulos.precio"));

        }while(rs.next());

        return grupo;
    }
}
