package es.cipfpbatoi.ud02a01.repository.csv.cliente;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

import org.springframework.stereotype.Repository;

import es.cipfpbatoi.ud02a01.model.Cliente;

@Repository
public class ClienteCsvDAO {

    public File writeCSV(List<Cliente> clientes) {

        File file;
        try {
            file = File.createTempFile("clientes_", ".csv");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        try
                (
                        OutputStream outputStream = new FileOutputStream(file);
                        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
                        BufferedWriter bufferedWriter = new BufferedWriter(writer);
                )
        {
            bufferedWriter.write("ID;NOMBRE;DIRECCIÓN\n");

            for (Cliente cliente:clientes) {
                bufferedWriter.write(cliente.getId() + ";" );
                bufferedWriter.write(cliente.getNombre() + ";");
                bufferedWriter.write(cliente.getDireccion() + "\n");
            }

            return file;
        } catch ( IOException e) {
            e.printStackTrace();
            return null;
        }

    }
}
