package es.cipfpbatoi.ud02a01.repository.jdbc.grupos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import es.cipfpbatoi.ud02a01.model.Articulos;
import es.cipfpbatoi.ud02a01.model.Grupos;
import org.springframework.jdbc.core.RowMapper;


public class GrupoWithArticulosRowMapper implements RowMapper<Grupos>{

    private Map<Integer, Grupos> grupos = new HashMap<>();

    @Override
    public Grupos mapRow(ResultSet rs, int rowNum) throws SQLException {

        Integer idGrupo = rs.getInt("grupos.id");
        Grupos grupo = this.grupos.get(idGrupo);

        if(grupo == null) {
            grupo = new Grupos();
            this.grupos.put(idGrupo, grupo);

            grupo.setId(idGrupo);
            grupo.setDescripcion(rs.getString("grupos.descripcion"));
        }

        Articulos articulo = new Articulos();
        grupo.addArticulo(articulo);

        articulo.setId(rs.getInt("articulos.id"));
        articulo.setIdGrupo(rs.getInt("grupos.id"));
        articulo.setCodigo(rs.getString("articulos.codigo"));
        articulo.setNombre(rs.getString("articulos.nombre"));
        articulo.setPrecio(rs.getDouble("articulos.precio"));

        return grupo;
    }

}

