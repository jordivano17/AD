package es.cipfpbatoi.ud02a01.repository.csv.grupos;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

import es.cipfpbatoi.ud02a01.model.Grupos;
import org.springframework.stereotype.Repository;

import es.cipfpbatoi.ud02a01.model.Cliente;

@Repository
public class GrupoCsvDAO {

    public File writeCSV(List<Grupos> grupos) {

        File file;
        try {
            file = File.createTempFile("grupos_", ".csv");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        try
                (
                        OutputStream outputStream = new FileOutputStream(file);
                        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
                        BufferedWriter bufferedWriter = new BufferedWriter(writer);
                )
        {
            bufferedWriter.write("ID;DESCRIPCION\n");

            for (Grupos grupos1:grupos) {
                bufferedWriter.write(grupos1.getId() + ";" );
                bufferedWriter.write(grupos1.getDescripcion() + "\n");
            }

            return file;
        } catch ( IOException e) {
            e.printStackTrace();
            return null;
        }

    }
}

