package es.cipfpbatoi.ud02a01.repository.jdbc.lineafacturas;

import es.cipfpbatoi.ud02a01.model.LineaFacturas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;

@Repository
public class LineaFacturasJDBCDAO {
    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String SELECT_COUNT = "SELECT COUNT(*) FROM LINEA_FACTURA";

    private static final String SELECT_ALL =  "SELECT * FROM LINEA_FACTURA";
    private static final String SELECT_BY_ID =  "SELECT * FROM LINEA_FACTURA WHERE LINEA = ?";

    private static final String INSERT =  "INSERT INTO LINEA_FACTURA (linea, factura, articulo, cantidad, importe) VALUES(?,?,?,?,?)";
    private static final String UPDATE =  "UPDATE LINEA_FACTURA SET factura=?, articulo=?, cantidad=?, importe=? WHERE linea=?";
    private static final String DELETE =  "DELETE LINEA_FACTURA WHERE linea=?";

    public int count(){return jdbcTemplate.queryForObject(LineaFacturasJDBCDAO.SELECT_COUNT,Integer.class);}

    public List<LineaFacturas> findAll() {

        List<LineaFacturas> Lineafacturas = this.jdbcTemplate.query(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        return connection.prepareStatement(SELECT_ALL, Statement.CLOSE_CURRENT_RESULT);

                    }

                },
                new LineaFacturasRowMapper()
        );

        return Lineafacturas;
    }

    public LineaFacturas findById(Integer id) {

        LineaFacturas Lineafacturas = this.jdbcTemplate.query(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                PreparedStatement ps =   connection.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
                ps.setInt(1, id);
                return ps;

            }

        }, new LineaFacturasResultSetExtractor());

        return Lineafacturas;
    }


    public LineaFacturas insert(LineaFacturas Lineafactu) {

        KeyHolder keyHolder = new GeneratedKeyHolder();

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =  connection.prepareStatement(
                                INSERT,
                                Statement.CLOSE_CURRENT_RESULT);
                        ps.setInt(1, Lineafactu.getLinea());
                        ps.setInt(2,Lineafactu.getFactura());
                        ps.setInt(3,Lineafactu.getArticulo());
                        ps.setInt(4,Lineafactu.getCantidad());
                        ps.setDouble(5,Lineafactu.getImporte());
                        return ps;
                    }
                }, keyHolder);

        if(resultadoOperacion != 1) {
            return null;
        }

        Lineafactu.setLinea(keyHolder.getKey().intValue());
        return Lineafactu;
    }

    public Boolean update(LineaFacturas Lineafacturas) {

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(
                                UPDATE,
                                Statement.CLOSE_CURRENT_RESULT);


                        ps.setInt(1, Lineafacturas.getArticulo());
                        ps.setInt(2, Lineafacturas.getFactura());
                        ps.setInt(3, Lineafacturas.getCantidad());
                        ps.setDouble(4,Lineafacturas.getImporte());
                        return ps;
                    }
                });

        return resultadoOperacion == 1;
    }

    public Boolean delete(LineaFacturas Lineafacturas) {

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(
                                DELETE,
                                Statement.CLOSE_CURRENT_RESULT);
                        ps.setInt(1, Lineafacturas.getLinea());
                        return ps;
                    }
                });

        return resultadoOperacion == 1;
    }
}
