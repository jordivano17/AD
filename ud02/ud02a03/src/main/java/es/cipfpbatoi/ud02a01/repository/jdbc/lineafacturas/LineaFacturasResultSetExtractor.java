package es.cipfpbatoi.ud02a01.repository.jdbc.lineafacturas;

import es.cipfpbatoi.ud02a01.model.Facturas;
import es.cipfpbatoi.ud02a01.model.LineaFacturas;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LineaFacturasResultSetExtractor implements ResultSetExtractor<LineaFacturas> {

    @Override
    public LineaFacturas extractData(ResultSet rs) throws SQLException, DataAccessException {

        if(!rs.next()) {
            return null;
        }

        LineaFacturas Lineafacturas = new LineaFacturas();

        Lineafacturas.setLinea(rs.getInt("linea"));
        Lineafacturas.setArticulo(rs.getInt("articulo"));
        Lineafacturas.setFactura(rs.getInt("factura"));
        Lineafacturas.setCantidad(rs.getInt("cantidad"));
        Lineafacturas.setImporte(rs.getDouble("importe"));

        return Lineafacturas;
    }
}
