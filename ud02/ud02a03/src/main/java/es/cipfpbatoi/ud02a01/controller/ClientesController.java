package es.cipfpbatoi.ud02a01.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import es.cipfpbatoi.ud02a01.repository.csv.cliente.ClienteCsvDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.repository.jdbc.cliente.ClienteJDBCDAO;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/clientes")
public class ClientesController {
	
	@Autowired
	ClienteJDBCDAO clienteJDBCDAO;

	@Autowired
	ClienteCsvDAO clienteCsvDAO;

	@PostMapping("")
	@ResponseStatus(code= HttpStatus.CREATED)
	public Cliente create(@RequestBody Cliente cliente) {

		Cliente clienteInsertado = this.clienteJDBCDAO.insert(cliente);
		if(clienteInsertado == null) {
			throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
					"Entity not created.");
		}

		return clienteInsertado;
	}

	@GetMapping("/count")
	public Integer count() {
		return this.clienteJDBCDAO.count();
	}

	@GetMapping("")
	public List<Cliente> findAll() {
		return this.clienteJDBCDAO.findAll();
	}

	@GetMapping(value="", params = "fileFormat")
	public ResponseEntity<Resource> findAll(@RequestParam String fileFormat) {
		List<Cliente> clientes = this.clienteJDBCDAO.findAll();
		File file = null;




		switch (fileFormat) {
			case "csv":
				file = this.clienteCsvDAO.writeCSV(clientes);
				break;

			default:
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
						"fileFormat not supported.");
		}

		if(file == null) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					"File not generated.");
		}

		Path path = Paths.get(file.getAbsolutePath());
		ByteArrayResource resource;
		try {
			resource = new ByteArrayResource(Files.readAllBytes(path));

			return ResponseEntity.ok()
					.header("Content-disposition", "attachment; filename="+ file.getName())
					.header("Cache-Control", "no-cache, no-store, must-revalidate")
					.header("Pragma", "no-cache")
					.header("Expires", "0")
					.contentLength(file.length())
					.contentType(MediaType.APPLICATION_OCTET_STREAM)
					.body(resource);
		}
		catch (IOException e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					"File cannot be downloaded.");
		}
		finally {
			file.delete();
		}
	}

	@GetMapping("/{id}")
	public Cliente findById(@PathVariable int id) {
		Cliente cliente = this.clienteJDBCDAO.findById(id);
		if(cliente == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"Entity not found.");
		}
		return cliente;
	}

	@PutMapping("/{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity updated" )
	public void update(@PathVariable int id, @RequestBody Cliente cliente) {

		boolean updated = this.clienteJDBCDAO.update(cliente);
		if(!updated) {
			throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
					"Entity not updated.");
		}
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity deleted" )
	public void delete(@PathVariable int id, @RequestBody Cliente cliente) {

		boolean deleted = this.clienteJDBCDAO.delete(cliente);
		if(!deleted) {
			throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
					"Entity not deleted.");
		}
	}
}
