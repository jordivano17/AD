package es.cipfpbatoi.ud02a01.controller;

import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.model.Vendedor;
import es.cipfpbatoi.ud02a01.repository.csv.vendedor.VendedorCsvDAO;
import es.cipfpbatoi.ud02a01.repository.jdbc.vendedor.VendedorJDBCDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping("/vendedores")
public class VendedoresController {

    @Autowired
    VendedorJDBCDAO vendedorJDBCDAO;

    @Autowired
    VendedorCsvDAO vendedorCsvDAO;

    @PostMapping("")
    @ResponseStatus(code= HttpStatus.CREATED)
    public Vendedor create(@RequestBody Vendedor vendedor) {

        Vendedor venInsertado = this.vendedorJDBCDAO.insert(vendedor);
        if(venInsertado == null) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not created.");
        }

        return venInsertado;
    }

    @GetMapping("/count")
    public Integer count() {
        return this.vendedorJDBCDAO.count();
    }

    @GetMapping("")
    public List<Vendedor> findAll() {
        return this.vendedorJDBCDAO.findAll();
    }

    @GetMapping(value="", params = "fileFormat")
    public ResponseEntity<Resource> findAll(@RequestParam String fileFormat) {
        List<Vendedor> vendedors = this.vendedorJDBCDAO.findAll();
        File file = null;




        switch (fileFormat) {
            case "csv":
                file = this.vendedorCsvDAO.writeCSV(vendedors);
                break;

            default:
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "fileFormat not supported.");
        }

        if(file == null) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "File not generated.");
        }

        Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource;
        try {
            resource = new ByteArrayResource(Files.readAllBytes(path));

            return ResponseEntity.ok()
                    .header("Content-disposition", "attachment; filename="+ file.getName())
                    .header("Cache-Control", "no-cache, no-store, must-revalidate")
                    .header("Pragma", "no-cache")
                    .header("Expires", "0")
                    .contentLength(file.length())
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        }
        catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "File cannot be downloaded.");
        }
        finally {
            file.delete();
        }
    }

    @GetMapping("/{id}")
    public Vendedor findById(@PathVariable int id) {
        Vendedor vendedor = this.vendedorJDBCDAO.findById(id);
        if(vendedor == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Entity not found.");
        }
        return vendedor;
    }

    @PutMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity updated" )
    public void update(@PathVariable int id, @RequestBody Vendedor vendedor) {

        boolean updated = this.vendedorJDBCDAO.update(vendedor);
        if(!updated) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not updated.");
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity deleted" )
    public void delete(@PathVariable int id, @RequestBody Vendedor vendedor) {

        boolean deleted = this.vendedorJDBCDAO.delete(vendedor);
        if(!deleted) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not deleted.");
        }
    }
}