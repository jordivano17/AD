package es.cipfpbatoi.ud02a01.repository.jdbc.grupos;

import es.cipfpbatoi.ud02a01.model.Grupos;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GruposRowMapper implements RowMapper<Grupos> {

    @Override
    public Grupos mapRow(ResultSet rs, int rowNum) throws SQLException {

        Grupos grupos = new Grupos();
        grupos.setId(rs.getInt("id"));
        grupos.setDescripcion(rs.getString("descripcion"));
        return grupos;

    }

}
