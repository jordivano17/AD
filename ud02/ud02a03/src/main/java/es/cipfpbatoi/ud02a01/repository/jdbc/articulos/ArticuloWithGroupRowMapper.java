package es.cipfpbatoi.ud02a01.repository.jdbc.articulos;

import es.cipfpbatoi.ud02a01.model.Articulos;
import es.cipfpbatoi.ud02a01.model.Grupos;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

class ArticuloWithGroupRowMapper implements RowMapper<Articulos> {

    @Override
    public Articulos mapRow(ResultSet rs, int rowNum) throws SQLException {

        Articulos articulo = new Articulos();

        articulo.setId(rs.getInt("articulos.id"));
        articulo.setIdGrupo(rs.getInt("grupos.id"));
        articulo.setCodigo(rs.getString("articulos.codigo"));
        articulo.setNombre(rs.getString("articulos.nombre"));
        articulo.setPrecio(rs.getDouble("articulos.precio"));
        articulo.setIdGrupo(rs.getInt("articulos.idGrupo"));

        Grupos grupo = new Grupos();
        articulo.setGrupo(grupo);

        grupo.setId(rs.getInt("grupos.id"));
        grupo.setDescripcion(rs.getString("grupos.descripcion"));

        return articulo;

    }

}
