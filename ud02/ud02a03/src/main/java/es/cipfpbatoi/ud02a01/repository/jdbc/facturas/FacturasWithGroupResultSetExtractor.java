package es.cipfpbatoi.ud02a01.repository.jdbc.facturas;

import es.cipfpbatoi.ud02a01.model.*;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

class FacturasWithGroupResultSetExtractor implements ResultSetExtractor<Facturas> {

    @Override
    public Facturas extractData(ResultSet rs) throws SQLException, DataAccessException {

        if(!rs.next()) {
            return null;
        }

        Facturas facturas = new Facturas();
        facturas.setId(rs.getInt("facturas.id"));
        facturas.setFecha(rs.getDate("facturas.fecha"));
        facturas.setCliente(null);
        facturas.setVendedor(null);
        facturas.setLineaFacturas(null);


        Cliente cliente = new Cliente();
        facturas.setCliente(cliente);
        cliente.setId(rs.getInt("clientes.id"));
        cliente.setDireccion(rs.getString("clientes.direccion"));
        cliente.setNombre(rs.getString("clientes.nombre"));


        Vendedor vendedor = new Vendedor();
        facturas.setVendedor(vendedor);
        vendedor.setId(rs.getInt("vendedores.id"));
        vendedor.setFechaIngreso(rs.getDate("vendedores.fecha_ingreso"));
        vendedor.setNombre(rs.getString("vendedores.nombre"));
        vendedor.setSalario(rs.getDouble("vendedores.salario"));

        LineaFacturas lineaFacturas = new LineaFacturas();
        facturas.setLineaFacturas(lineaFacturas);
        lineaFacturas.setLinea(rs.getInt("linea_factura.linea"));
        lineaFacturas.setArticulo(rs.getInt("linea_factura.articulo"));
        lineaFacturas.setFactura(rs.getInt("linea_factura.factura"));
        lineaFacturas.setCantidad(rs.getInt("linea_factura.cantidad"));
        lineaFacturas.setImporte(rs.getDouble("linea_factura.importe"));

        return facturas;
    }
}