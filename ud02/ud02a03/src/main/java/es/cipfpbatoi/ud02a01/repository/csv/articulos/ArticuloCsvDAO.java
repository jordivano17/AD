package es.cipfpbatoi.ud02a01.repository.csv.articulos;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import es.cipfpbatoi.ud02a01.model.Articulos;
import org.springframework.stereotype.Repository;

@Repository
public class ArticuloCsvDAO {

    public File writeCSV(List<Articulos> articulos) {

        File file;
        try {
            file = File.createTempFile("articulos_", ".csv");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        try
                (
                        OutputStream outputStream = new FileOutputStream(file);
                        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
                        BufferedWriter bufferedWriter = new BufferedWriter(writer);
                )
        {
            bufferedWriter.write("ID;NOMBRE;CODIGO;IDGRUPO;GRUPO;PRECIO\n");

            for (Articulos articulos1:articulos) {
                bufferedWriter.write(articulos1.getId() + ";" );
                bufferedWriter.write(articulos1.getNombre() + ";");
                bufferedWriter.write(articulos1.getCodigo() + ";");
                bufferedWriter.write(articulos1.getIdGrupo() + ";");
                bufferedWriter.write(articulos1.getGrupo() + ";");
                bufferedWriter.write(articulos1.getPrecio() + "\n");
            }

            return file;
        } catch ( IOException e) {
            e.printStackTrace();
            return null;
        }

    }
}

