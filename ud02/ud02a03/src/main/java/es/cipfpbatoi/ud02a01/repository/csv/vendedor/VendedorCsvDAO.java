package es.cipfpbatoi.ud02a01.repository.csv.vendedor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

import es.cipfpbatoi.ud02a01.model.Vendedor;
import org.springframework.stereotype.Repository;

import es.cipfpbatoi.ud02a01.model.Cliente;

@Repository
public class VendedorCsvDAO {

    public File writeCSV(List<Vendedor> vendedors) {

        File file;
        try {
            file = File.createTempFile("vendedores_", ".csv");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        try
                (
                        OutputStream outputStream = new FileOutputStream(file);
                        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
                        BufferedWriter bufferedWriter = new BufferedWriter(writer);
                )
        {
            bufferedWriter.write("ID;NOMBRE;SALARIO;FECHA_INGRESO\n");

            for (Vendedor vendedor:vendedors) {
                bufferedWriter.write(vendedor.getId() + ";" );
                bufferedWriter.write(vendedor.getNombre() + ";");
                bufferedWriter.write(vendedor.getSalario() + ";");
                bufferedWriter.write(vendedor.getFechaIngreso() + "\n");
            }

            return file;
        } catch ( IOException e) {
            e.printStackTrace();
            return null;
        }

    }
}

