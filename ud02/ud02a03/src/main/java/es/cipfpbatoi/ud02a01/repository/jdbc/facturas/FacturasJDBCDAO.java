package es.cipfpbatoi.ud02a01.repository.jdbc.facturas;

import es.cipfpbatoi.ud02a01.model.Articulos;
import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.model.Facturas;
import es.cipfpbatoi.ud02a01.model.Vendedor;
import es.cipfpbatoi.ud02a01.repository.jdbc.articulos.ArticulosResultSetExtractor;
import es.cipfpbatoi.ud02a01.repository.jdbc.articulos.ArticulosRowMapper;
import es.cipfpbatoi.ud02a01.repository.jdbc.articulos.ArticulosWithGroupResultSetExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;

@Repository
public class FacturasJDBCDAO {
    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String SELECT_COUNT = "SELECT COUNT(*) FROM FACTURAS";

    private static final String SELECT_ALL =  "SELECT * FROM FACTURAS";
    private static final String SELECT_BY_ID =  "SELECT * FROM FACTURAS WHERE ID = ?";
    private static final String SELECT_BY_ID_WITH_GROUP = "SELECT * FROM FACTURAS AS f " +
            "INNER JOIN lineas_factura AS l ON (f.id = l.factura)" +
            "INNER JOIN clientes AS c ON (f.cliente = c.id)" +
            "INNER JOIN vendedores AS v ON (f.vendedor = v.id)" +
            " WHERE f.id = ?";

    private static final String INSERT =  "INSERT INTO FACTURAS(id, fecha, cliente, vendedor, formapago) VALUES(?,?)";
    private static final String UPDATE =  "UPDATE FACTURAS SET fecha=?, cliente=?, vendedor=?, formapago=? WHERE id=?";
    private static final String DELETE =  "DELETE FACTURAS WHERE id=?";

    public int count(){return jdbcTemplate.queryForObject(es.cipfpbatoi.ud02a01.repository.jdbc.facturas.FacturasJDBCDAO.SELECT_COUNT,Integer.class);}

    public List<Facturas> findAll() {

        List<Facturas> facturas = this.jdbcTemplate.query(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        return connection.prepareStatement(SELECT_ALL, Statement.CLOSE_CURRENT_RESULT);

                    }

                },
                new FacturasRowMapper()
        );

        return facturas;
    }

    public Facturas findById(Integer id) {

        Facturas facturas = this.jdbcTemplate.query(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                PreparedStatement ps =   connection.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
                ps.setInt(1, id);
                return ps;

            }

        }, new FacturasResultSetExtractor());

        return facturas;
    }

    public Facturas findByIdWithGrupo(Integer id) {

        Facturas facturas = this.jdbcTemplate.query(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps = connection.prepareStatement(
                                SELECT_BY_ID_WITH_GROUP,
                                Statement.CLOSE_CURRENT_RESULT);
                        ps.setInt(1, id);
                        return ps;
                    }
                },
                new FacturasWithGroupResultSetExtractor()
        );

        return facturas;
    }


    public Facturas insert(Facturas factu) {

        KeyHolder keyHolder = new GeneratedKeyHolder();

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =  connection.prepareStatement(
                                INSERT,
                                Statement.CLOSE_CURRENT_RESULT);

                        ps.setInt(1, factu.getId());
                        ps.setDate(2, (Date)factu.getFecha());
                        ps.setObject(3,(Cliente)factu.getCliente());
                        ps.setObject(4,(Vendedor)factu.getVendedor());
                        ps.setString(5,factu.getFormapago());
                        return ps;
                    }
                }, keyHolder);

        if(resultadoOperacion != 1) {
            return null;
        }

        factu.setId(keyHolder.getKey().intValue());
        return factu;
    }

    public Boolean update(Facturas facturas) {

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(
                                UPDATE,
                                Statement.CLOSE_CURRENT_RESULT);
                        ps.setDate(1, (Date)facturas.getFecha());
                        ps.setObject(2,(Cliente)facturas.getCliente());
                        ps.setObject(3,(Vendedor)facturas.getVendedor());
                        ps.setString(4,facturas.getFormapago());
                        return ps;
                    }
                });

        return resultadoOperacion == 1;
    }

    public Boolean delete(Facturas facturas) {

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(
                                DELETE,
                                Statement.CLOSE_CURRENT_RESULT);
                        ps.setInt(1, facturas.getId());
                        return ps;
                    }
                });

        return resultadoOperacion == 1;
    }
}
