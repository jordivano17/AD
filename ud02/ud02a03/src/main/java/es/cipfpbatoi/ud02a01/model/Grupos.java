package es.cipfpbatoi.ud02a01.model;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Grupos {

    private int id;
    private String descripcion;

    private List<Articulos> articulos = new ArrayList<>();

    public Grupos() {
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Articulos> getArticulos() {
        return articulos;
    }
    public void setArticulos(List<Articulos> articulos) {
        this.articulos = articulos;
    }
    public void addArticulo(Articulos articulo) {
        this.articulos.add(articulo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Grupos other = (Grupos) obj;
        return id == other.id;
    }
}

