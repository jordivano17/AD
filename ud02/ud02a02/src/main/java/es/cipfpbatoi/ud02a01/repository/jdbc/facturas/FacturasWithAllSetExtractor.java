package es.cipfpbatoi.ud02a01.repository.jdbc.facturas;

import java.sql.ResultSet;
import java.sql.SQLException;

import es.cipfpbatoi.ud02a01.model.*;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import es.cipfpbatoi.ud02a01.model.Articulos;
import es.cipfpbatoi.ud02a01.model.Facturas;
import es.cipfpbatoi.ud02a01.model.LineaFacturas;

public class FacturasWithAllSetExtractor implements ResultSetExtractor<Facturas> {

    //Obtenemos solo la factura deseada con toda la informacion, vamos metiendo las lineas en el array siempre que hayan
    @Override
    public Facturas extractData(ResultSet rs) throws SQLException, DataAccessException {
        // TODO Auto-generated method stub

        if(!rs.next()) {
            return null;
        }


        Facturas factura= new Facturas();
        factura.setId(rs.getInt("facturas.id"));
        factura.setFecha(rs.getDate("facturas.fecha"));
        factura.setFormapago(rs.getString("facturas.formapago"));
        factura.setCliente(rs.getInt("facturas.cliente"));
        factura.setVendedor(rs.getInt("facturas.vendedor"));

        Cliente cliente= new Cliente();
        cliente.setId(rs.getInt("clientes.id"));
        cliente.setNombre(rs.getString("clientes.nombre"));
        cliente.setDireccion(rs.getString("clientes.direccion"));

        factura.setInfoCliente(cliente);

        Vendedor vendedor= new Vendedor();
        vendedor.setNombre(rs.getString("vendedores.nombre"));
        vendedor.setId(rs.getInt("facturas.vendedor"));

        factura.setInfoVendedor(vendedor);


        do {

            LineaFacturas linea= new LineaFacturas();

            linea.setLinea(rs.getInt("lineas_factura.linea"));
            linea.setFactura(rs.getInt("lineas_factura.factura"));
            linea.setIdArticulo(rs.getInt("lineas_factura.articulo"));
            linea.setCantidad(rs.getInt("lineas_factura.cantidad"));
            linea.setImporte(rs.getFloat("lineas_factura.importe"));

            Articulos articulo= new Articulos();

            articulo.setId(rs.getInt("articulos.id"));
            articulo.setCodigo(rs.getString("articulos.codigo"));
            articulo.setIdGrupo(rs.getInt("articulos.grupo"));
            articulo.setNombre(rs.getString("articulos.nombre"));
            articulo.setPrecio(rs.getFloat("articulos.precio"));

            linea.setArticulo(articulo);

            factura.addLineaFacturas(linea);
        } while (rs.next());



        return factura;
    }

}

