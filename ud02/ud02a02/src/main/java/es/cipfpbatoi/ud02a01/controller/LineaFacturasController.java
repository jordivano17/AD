package es.cipfpbatoi.ud02a01.controller;

import es.cipfpbatoi.ud02a01.model.LineaFacturas;
import es.cipfpbatoi.ud02a01.repository.jdbc.lineafacturas.LineaFacturasJDBCDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/lineafactura")
public class LineaFacturasController {
    @Autowired
    LineaFacturasJDBCDAO lineaFacturasJDBCDAO;
    @PostMapping("")
    @ResponseStatus(code= HttpStatus.CREATED)
    public LineaFacturas create(@RequestBody LineaFacturas Lineafacturas) {

        LineaFacturas Lineafactinsertado = this.lineaFacturasJDBCDAO.insert(Lineafacturas);
        if(Lineafactinsertado == null) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not created.");
        }

        return Lineafactinsertado;
    }

    @GetMapping("/count")
    public Integer count() {
        return this.lineaFacturasJDBCDAO.count();
    }

    @GetMapping("")
    public List<LineaFacturas> findAll() {
        return this.lineaFacturasJDBCDAO.findAll();
    }

    @GetMapping("/{id}")
    public LineaFacturas findById(@PathVariable int id) {
        LineaFacturas Lineafacturas = this.lineaFacturasJDBCDAO.findById(id);
        if(Lineafacturas == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Entity not found.");
        }
        return Lineafacturas;
    }

    @PutMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity updated" )
    public void update(@PathVariable int id, @RequestBody LineaFacturas Lineafacturas) {

        boolean updated = this.lineaFacturasJDBCDAO.update(Lineafacturas);
        if(!updated) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not updated.");
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity deleted" )
    public void delete(@PathVariable int id, @RequestBody LineaFacturas Lineafacturas) {

        boolean deleted = this.lineaFacturasJDBCDAO.delete(Lineafacturas);
        if(!deleted) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not deleted.");
        }
    }
}