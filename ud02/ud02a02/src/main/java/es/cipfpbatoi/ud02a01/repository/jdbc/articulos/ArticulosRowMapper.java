package es.cipfpbatoi.ud02a01.repository.jdbc.articulos;

import es.cipfpbatoi.ud02a01.model.Articulos;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ArticulosRowMapper implements RowMapper<Articulos> {

    @Override
    public Articulos mapRow(ResultSet rs, int rowNum) throws SQLException {

        if(!rs.next()) {
            return null;
        }

        Articulos articulos = new Articulos();

        articulos.setId(rs.getInt("id"));
        articulos.setNombre(rs.getString("nombre"));
        articulos.setCodigo(rs.getString("codigo"));
        articulos.setPrecio(rs.getDouble("precio"));
        articulos.setIdGrupo(rs.getInt("idGrupo"));
        articulos.setGrupo(null);

        return articulos;
    }
}
