package es.cipfpbatoi.ud02a01.repository.jdbc.lineafacturas;

import es.cipfpbatoi.ud02a01.model.LineaFacturas;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LineaFacturasRowMapper implements RowMapper<LineaFacturas> {

    @Override
    public LineaFacturas mapRow(ResultSet rs, int rowNum) throws SQLException {

        if(!rs.next()) {
            return null;
        }

        LineaFacturas Lineafacturas = new LineaFacturas();

        Lineafacturas.setLinea(rs.getInt("linea"));
        Lineafacturas.setArticulo(rs.getInt("articulo"));
        Lineafacturas.setFactura(rs.getInt("factura"));
        Lineafacturas.setCantidad(rs.getInt("cantidad"));
        Lineafacturas.setImporte(rs.getDouble("importe"));

        return Lineafacturas;
    }
}