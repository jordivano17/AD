package es.cipfpbatoi.ud02a01.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Facturas {

    private int id;
    private Date fecha;
    private String formapago;
    private int cliente;
    private int vendedor;

    private Cliente infoCliente;
    private Vendedor infoVendedor;
    private List<LineaFacturas> lineaFacturas = new ArrayList<>();

    public String getFormapago() {
        return formapago;
    }

    public void setFormapago(String formapago) {
        this.formapago = formapago;
    }

    public Facturas() {
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getCliente() {
        return cliente;
    }

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    public void setVendedor(int vendedor) {
        this.vendedor = vendedor;
    }

    public int getVendedor() {
        return vendedor;
    }

    public List<LineaFacturas> getLineaFacturas() {
        return lineaFacturas;
    }

    public void setLineaFacturas(List<LineaFacturas> lineaFacturas) {
        this.lineaFacturas = lineaFacturas;
    }

    public void addLineaFacturas(List<LineaFacturas> lineaFacturas){
        this.lineaFacturas = lineaFacturas;
    }

    public Cliente getInfoCliente() {
        return infoCliente;
    }

    public void setInfoCliente(Cliente infoCliente) {
        this.infoCliente = infoCliente;
    }

    public Vendedor getInfoVendedor() {
        return infoVendedor;
    }

    public void setInfoVendedor(Vendedor infoVendedor) {
        this.infoVendedor = infoVendedor;
    }
}
