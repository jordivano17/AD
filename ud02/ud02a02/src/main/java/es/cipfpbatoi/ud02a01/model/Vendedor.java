package es.cipfpbatoi.ud02a01.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Vendedor {

    private int id;
    private String nombre;
    private Date fechaIngreso;
    private double salario;
    private Vendedor nombres;


    public Vendedor() {
    }

    public Vendedor(int id, String nombre, Date fechaIngreso, double salario) {
        super();
        this.id = id;
        this.nombre = nombre;
        this.fechaIngreso = fechaIngreso;
        this.salario = salario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    //public Vendedor getNombres() {
    //    return nombres;
    //}

    //public void setNombres(Vendedor nombres) {
    //    this.nombres = nombres;
    //}
}
