package es.cipfpbatoi.ud02a01.controller;


import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.model.Grupos;
import es.cipfpbatoi.ud02a01.repository.jdbc.grupos.GruposJDBCDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/grupos")
public class GruposController {
    @Autowired
    GruposJDBCDAO gruposJDBCDAO;
    @PostMapping("")
    @ResponseStatus(code= HttpStatus.CREATED)
    public Grupos create(@RequestBody Grupos grupos) {

        Grupos grupoinsertado = this.gruposJDBCDAO.insert(grupos);
        if(grupoinsertado == null) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not created.");
        }

        return grupoinsertado;
    }

    @GetMapping("/count")
    public Integer count() {
        return this.gruposJDBCDAO.count();
    }

    @GetMapping("/")
    public List<Grupos> findAll() {
        return this.gruposJDBCDAO.findAll();
    }

    @GetMapping("/{id}")
    public Grupos findById(@PathVariable int id) {
        Grupos grupos = this.gruposJDBCDAO.findById(id);
        if(grupos == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Entity not found.");
        }
        return grupos;
    }

    @PutMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity updated" )
    public void update(@PathVariable int id, @RequestBody Grupos grupos) {

        boolean updated = this.gruposJDBCDAO.update(grupos);
        if(!updated) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not updated.");
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity deleted" )
    public void delete(@PathVariable int id, @RequestBody Grupos grupos) {

        boolean deleted = this.gruposJDBCDAO.delete(grupos);
        if(!deleted) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not deleted.");
        }
    }
}
