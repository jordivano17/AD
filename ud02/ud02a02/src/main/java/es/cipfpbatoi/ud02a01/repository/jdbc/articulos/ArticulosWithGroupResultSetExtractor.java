package es.cipfpbatoi.ud02a01.repository.jdbc.articulos;

import es.cipfpbatoi.ud02a01.model.Articulos;
import es.cipfpbatoi.ud02a01.model.Grupos;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ArticulosWithGroupResultSetExtractor implements ResultSetExtractor<Articulos> {

    @Override
    public Articulos extractData(ResultSet rs) throws SQLException, DataAccessException {

        if(!rs.next()) {
            return null;
        }

        Articulos articulo = new Articulos();

        articulo.setId(rs.getInt("articulos.id"));
        articulo.setIdGrupo(rs.getInt("grupos.id"));
        articulo.setCodigo(rs.getString("articulos.codigo"));
        articulo.setNombre(rs.getString("articulos.nombre"));
        articulo.setPrecio(rs.getDouble("articulos.precio"));
        articulo.setIdGrupo(rs.getInt("articulos.grupo"));

        Grupos grupo = new Grupos();
        articulo.setGrupo(grupo);

        grupo.setId(rs.getInt("grupos.id"));
        grupo.setDescripcion(rs.getString("grupos.descripcion"));

        return articulo;
    }
}