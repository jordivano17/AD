package es.cipfpbatoi.ud02a01.repository.jdbc.articulos;

import es.cipfpbatoi.ud02a01.model.Articulos;
import es.cipfpbatoi.ud02a01.model.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Repository
public class ArticulosJDBCDAO {
    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String SELECT_COUNT = "SELECT COUNT(*) FROM ARTICULOS";

    private static final String SELECT_ALL =  "SELECT * FROM ARTICULOS";
    private static final String SELECT_BY_ID =  "SELECT * FROM ARTICULOS WHERE ID = ?";
    private static final String SELECT_BY_ID_WITH_GROUP = "SELECT * FROM articulos AS a " +
            "INNER JOIN grupos AS g ON (a.GRUPO = g.ID) WHERE a.ID = ?";

    private static final String INSERT =  "INSERT INTO ARTICULOS(id, nombre, precio, codigo, grupo) VALUES(?,?,?,?,?)";
    private static final String UPDATE =  "UPDATE ARTICULOS SET nombre=?, precio=?, codigo=?, grupo=? WHERE id=?";
    private static final String DELETE =  "DELETE ARTICULOS WHERE id=?";

    public int count(){return jdbcTemplate.queryForObject(ArticulosJDBCDAO.SELECT_COUNT,Integer.class);}

    public List<Articulos> findAll() {

        List<Articulos> articulos = this.jdbcTemplate.query(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        return connection.prepareStatement(SELECT_ALL, Statement.CLOSE_CURRENT_RESULT);

                    }

                },
                new ArticulosRowMapper()
        );

        return articulos;
    }

    public Articulos findById(Integer id) {

        Articulos articulos = this.jdbcTemplate.query(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                PreparedStatement ps =   connection.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
                ps.setInt(1, id);
                return ps;

            }

        }, new ArticulosResultSetExtractor());

        return articulos;
    }

    public Articulos findByIdWithGrupo(Integer id) {

            Articulos articulo = this.jdbcTemplate.query(
                    new PreparedStatementCreator() {
                        @Override
                        public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                            PreparedStatement ps = connection.prepareStatement(
                                    SELECT_BY_ID_WITH_GROUP,
                                    Statement.CLOSE_CURRENT_RESULT);
                            ps.setInt(1, id);
                            return ps;
                        }
                    },
                    new ArticulosWithGroupResultSetExtractor()
            );

            return articulo;
        }


    public Articulos insert(Articulos articulos) {

        KeyHolder keyHolder = new GeneratedKeyHolder();

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =  connection.prepareStatement(
                                INSERT,
                                Statement.CLOSE_CURRENT_RESULT);

                        ps.setInt(1,articulos.getId());
                        ps.setString(2, articulos.getNombre());
                        ps.setDouble(3,articulos.getPrecio());
                        ps.setString(4, articulos.getCodigo());
                        ps.setInt(5,articulos.getIdGrupo());
                        return ps;
                    }
                }, keyHolder);

        if(resultadoOperacion != 1) {
            return null;
        }

        articulos.setId(keyHolder.getKey().intValue());
        return articulos;
    }

    public Boolean update(Articulos articulos) {

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(
                                UPDATE,
                                Statement.CLOSE_CURRENT_RESULT);
                        ps.setString(1, articulos.getNombre());
                        ps.setDouble(2,articulos.getPrecio());
                        ps.setString(3, articulos.getCodigo());
                        ps.setInt(4,articulos.getIdGrupo());
                        return ps;
                    }
                });

        return resultadoOperacion == 1;
    }

    public Boolean delete(Articulos articulos) {

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(
                                DELETE,
                                Statement.CLOSE_CURRENT_RESULT);
                        ps.setInt(1, articulos.getId());
                        return ps;
                    }
                });

        return resultadoOperacion == 1;
    }
}
