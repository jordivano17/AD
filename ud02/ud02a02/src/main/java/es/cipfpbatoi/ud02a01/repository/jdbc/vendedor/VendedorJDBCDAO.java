package es.cipfpbatoi.ud02a01.repository.jdbc.vendedor;


import es.cipfpbatoi.ud02a01.model.Articulos;
import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.model.Vendedor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;

@Repository
    public class VendedorJDBCDAO{
        @Autowired
        JdbcTemplate jdbcTemplate;

        private static final String SELECT_COUNT = "SELECT COUNT(*) FROM VENDEDORES";

        private static final String SELECT_ALL =  "SELECT * FROM VENDEDORES";
        private static final String SELECT_BY_ID =  "SELECT * FROM VENDEDORES WHERE id = ?";

    private static final String INSERT =  "INSERT INTO VENDEDORES(id, nombre, fecha_ingreso, salario) VALUES(?,?,?,?)";
    private static final String UPDATE =  "UPDATE VENDEDORES SET nombre=?, fecha_ingreso=?, salario=? WHERE id=?";
    private static final String DELETE =  "DELETE VENDEDORES WHERE id=?";



        public int count(){
            return jdbcTemplate.queryForObject(VendedorJDBCDAO.SELECT_COUNT, Integer.class);
        }

    public List<Vendedor> findAll() {

        List<Vendedor> vendedores = this.jdbcTemplate.query(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        return connection.prepareStatement(SELECT_ALL, Statement.CLOSE_CURRENT_RESULT);

                    }

                },
                new VendedorRowMapper()
        );

        return vendedores;
    }

    public Vendedor findById(Integer id) {

        Vendedor vendedores = this.jdbcTemplate.query(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                PreparedStatement ps =   connection.prepareStatement(SELECT_BY_ID, Statement.CLOSE_CURRENT_RESULT);
                ps.setInt(1, id);
                return ps;

            }

        }, new VendedorResultSetExtractor());

        return vendedores;
    }

    public Vendedor insert(Vendedor vendedor) {

        KeyHolder keyHolder = new GeneratedKeyHolder();

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =  connection.prepareStatement(
                                INSERT,
                                Statement.CLOSE_CURRENT_RESULT);

                        ps.setInt(1,vendedor.getId());
                        ps.setString(2, vendedor.getNombre());
                        ps.setDate(3, (Date) vendedor.getFechaIngreso());
                        ps.setDouble(4,vendedor.getSalario());
                        return ps;
                    }
                }, keyHolder);

        if(resultadoOperacion != 1) {
            return null;
        }

        vendedor.setId(keyHolder.getKey().intValue());
        return vendedor;
    }

    public Boolean update(Vendedor vendedor) {

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(
                                UPDATE,
                                Statement.CLOSE_CURRENT_RESULT);

                        ps.setString(1, vendedor.getNombre());
                        ps.setDate(2, (Date) vendedor.getFechaIngreso());
                        ps.setDouble(3,vendedor.getSalario());
                        return ps;
                    }
                });

        return resultadoOperacion == 1;
    }

    public Boolean delete(Vendedor vendedor) {

        int resultadoOperacion = this.jdbcTemplate.update(
                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                        PreparedStatement ps =   connection.prepareStatement(
                                DELETE,
                                Statement.CLOSE_CURRENT_RESULT);
                        ps.setInt(1, vendedor.getId());
                        return ps;
                    }
                });

        return resultadoOperacion == 1;
    }
}
