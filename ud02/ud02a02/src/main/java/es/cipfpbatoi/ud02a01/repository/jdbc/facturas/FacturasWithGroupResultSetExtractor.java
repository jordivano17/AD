package es.cipfpbatoi.ud02a01.repository.jdbc.facturas;

import es.cipfpbatoi.ud02a01.model.*;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

class FacturasWithGroupResultSetExtractor implements ResultSetExtractor<Facturas> {

    @Override
    public Facturas extractData(ResultSet rs) throws SQLException, DataAccessException {

        if(!rs.next()) {
            return null;
        }

        Facturas facturas = new Facturas();
        facturas.setId(rs.getInt("facturas.id"));
        facturas.setFecha(rs.getDate("facturas.fecha"));
        facturas.setCliente(rs.getInt("facturas.cliente"));
        facturas.setVendedor(rs.getInt("facturas.cliente"));
        facturas.setFormapago(rs.getString("factuars.cliente"));


        Cliente cliente = new Cliente();
        cliente.setId(rs.getInt("clientes.id"));
        cliente.setDireccion(rs.getString("clientes.direccion"));
        cliente.setNombre(rs.getString("clientes.nombre"));

        facturas.setInfoCliente(cliente);

        Vendedor vendedor = new Vendedor();
        vendedor.setId(rs.getInt("vendedores.id"));
        vendedor.setFechaIngreso(rs.getDate("vendedores.fecha_ingreso"));
        vendedor.setNombre(rs.getString("vendedores.nombre"));
        vendedor.setSalario(rs.getDouble("vendedores.salario"));

        do {

            LineaFacturas linea= new LineaFacturas();

            linea.setLinea(rs.getInt("lineas_factura.linea"));
            linea.setFactura(rs.getInt("lineas_factura.factura"));
            linea.setIdArticulo(rs.getInt("lineas_factura.articulo"));
            linea.setCantidad(rs.getInt("lineas_factura.cantidad"));
            linea.setImporte(rs.getFloat("lineas_factura.importe"));

            Articulos articulo= new Articulos();

            articulo.setId(rs.getInt("articulos.id"));
            articulo.setCodigo(rs.getString("articulos.codigo"));
            articulo.setIdGrupo(rs.getInt("articulos.grupo"));
            articulo.setNombre(rs.getString("articulos.nombre"));
            articulo.setPrecio(rs.getFloat("articulos.precio"));

            linea.setArticulo(articulo);

            facturas.addLineaFacturas(linea);
        } while (rs.next());

        return facturas;
    }
}