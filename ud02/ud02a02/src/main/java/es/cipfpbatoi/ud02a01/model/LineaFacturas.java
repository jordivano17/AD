package es.cipfpbatoi.ud02a01.model;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class LineaFacturas implements List<LineaFacturas> {

    private int linea;
    private int factura;
    private int idArticulo;
    private int cantidad;
    private double importe;

    private Articulos articulo;

    public LineaFacturas() {
    }

    public int getLinea() {
        return linea;
    }

    public void setLinea(int linea) {
        this.linea = linea;
    }

    public int getFactura() {
        return factura;
    }

    public void setFactura(int factura) {
        this.factura = factura;
    }

    public int getIdArticulo() {
        return idArticulo;
    }

    public void setArticulo(int idArticulo) {
        this.idArticulo = idArticulo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }

    public void setIdArticulo(int idArticulo) {
        this.idArticulo = idArticulo;
    }

    public Articulos getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulos articulo) {
        this.articulo = articulo;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<LineaFacturas> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(LineaFacturas lineaFacturas) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends LineaFacturas> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends LineaFacturas> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public LineaFacturas get(int index) {
        return null;
    }

    @Override
    public LineaFacturas set(int index, LineaFacturas element) {
        return null;
    }

    @Override
    public void add(int index, LineaFacturas element) {

    }

    @Override
    public LineaFacturas remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<LineaFacturas> listIterator() {
        return null;
    }

    @Override
    public ListIterator<LineaFacturas> listIterator(int index) {
        return null;
    }

    @Override
    public List<LineaFacturas> subList(int fromIndex, int toIndex) {
        return null;
    }
}
