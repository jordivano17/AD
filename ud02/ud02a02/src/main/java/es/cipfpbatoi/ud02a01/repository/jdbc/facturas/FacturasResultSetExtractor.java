package es.cipfpbatoi.ud02a01.repository.jdbc.facturas;

import es.cipfpbatoi.ud02a01.model.Articulos;
import es.cipfpbatoi.ud02a01.model.Facturas;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FacturasResultSetExtractor implements ResultSetExtractor<Facturas> {

    @Override
    public Facturas extractData(ResultSet rs) throws SQLException, DataAccessException {

        if(!rs.next()) {
            return null;
        }

        Facturas facturas = new Facturas();

        facturas.setId(rs.getInt("id"));
        facturas.setFormapago(rs.getString("formapago"));
        facturas.setFecha(rs.getDate("fecha"));
        facturas.setCliente(rs.getInt("cliente"));
        facturas.setVendedor(rs.getInt("vendedor"));

        return facturas;
    }
}
