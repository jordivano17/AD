package es.cipfpbatoi.ud02a01.controller;

import es.cipfpbatoi.ud02a01.model.Vendedor;
import es.cipfpbatoi.ud02a01.repository.jdbc.vendedor.VendedorJDBCDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/vendedores")
public class VendedoresController {

    @Autowired
    VendedorJDBCDAO vendedorJDBCDAO;
    @PostMapping("")
    @ResponseStatus(code= HttpStatus.CREATED)
    public Vendedor create(@RequestBody Vendedor vendedor) {

        Vendedor vendedorinsertado = this.vendedorJDBCDAO.insert(vendedor);
        if(vendedorinsertado == null) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not created.");
        }

        return vendedorinsertado;
    }

    @GetMapping("/count")
    public Integer count() {
        return this.vendedorJDBCDAO.count();
    }

    @GetMapping("/")
    public List<Vendedor> findAll() {
        return this.vendedorJDBCDAO.findAll();
    }

    @GetMapping("/{id}")
    public Vendedor findById(@PathVariable int id) {
        Vendedor v = this.vendedorJDBCDAO.findById(id);
        if(v == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Entity not found.");
        }
        return v;
    }

    @PutMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity updated" )
    public void update(@PathVariable int id, @RequestBody Vendedor vendedor) {

        boolean updated = this.vendedorJDBCDAO.update(vendedor);
        if(!updated) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not updated.");
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity deleted" )
    public void delete(@PathVariable int id, @RequestBody Vendedor vendedor) {

        boolean deleted = this.vendedorJDBCDAO.delete(vendedor);
        if(!deleted) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not deleted.");
        }
    }
}
