package es.cipfpbatoi.ud02a01.controller;


import es.cipfpbatoi.ud02a01.model.Articulos;
import es.cipfpbatoi.ud02a01.model.Facturas;
import es.cipfpbatoi.ud02a01.repository.jdbc.facturas.FacturasJDBCDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/facturas")
public class FacturasController {
    @Autowired
    FacturasJDBCDAO facturasJDBCDAO;
    @PostMapping("")
    @ResponseStatus(code= HttpStatus.CREATED)
    public Facturas create(@RequestBody Facturas facturas) {

        Facturas factinsertado = this.facturasJDBCDAO.insert(facturas);
        if(factinsertado == null) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not created.");
        }

        return factinsertado;
    }

    @GetMapping("/count")
    public Integer count() {
        return this.facturasJDBCDAO.count();
    }

    @GetMapping("/")
    public List<Facturas> findAll() {
        return this.facturasJDBCDAO.findAll();
    }


    @PutMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity updated" )
    public void update(@PathVariable int id, @RequestBody Facturas facturas) {

        boolean updated = this.facturasJDBCDAO.update(facturas);
        if(!updated) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not updated.");
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity deleted" )
    public void delete(@PathVariable int id, @RequestBody Facturas facturas) {

        boolean deleted = this.facturasJDBCDAO.delete(facturas);
        if(!deleted) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not deleted.");
        }
    }

    @GetMapping(value="/{id}")
    public Facturas findById(@PathVariable int id,
                              @RequestParam(required = false,
                                      defaultValue = "false")
                                      Boolean withGroup) {

        Facturas facturas = null;

        if(withGroup) {
            facturas = this.facturasJDBCDAO.findByIdWithGrupo(id);
        } else {
            facturas = this.facturasJDBCDAO.findById(id);
        }

        if(facturas == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Entity not found.");
        }
        return facturas;
    }
}