package es.cipfpbatoi.ud02a01.repository.jdbc.facturas;

import es.cipfpbatoi.ud02a01.model.Articulos;
import es.cipfpbatoi.ud02a01.model.Facturas;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FacturasRowMapper implements RowMapper<Facturas> {

    @Override
    public Facturas mapRow(ResultSet rs, int rowNum) throws SQLException {

        if(!rs.next()) {
            return null;
        }

        Facturas facturas = new Facturas();

        facturas.setId(rs.getInt("id"));
        facturas.setFecha(rs.getDate("fecha"));
        facturas.setCliente(rs.getInt("cliente"));
        facturas.setVendedor(rs.getInt("vendedor"));
        facturas.setFormapago(rs.getString("formapago"));

        return facturas;
    }
}