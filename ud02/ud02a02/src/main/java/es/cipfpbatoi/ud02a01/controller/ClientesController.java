package es.cipfpbatoi.ud02a01.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.repository.jdbc.cliente.ClienteJDBCDAO;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/clientes")
public class ClientesController {
	
	@Autowired
	ClienteJDBCDAO clienteJDBCDAO;


		@PostMapping("")
		@ResponseStatus(code= HttpStatus.CREATED)
		public Cliente create(@RequestBody Cliente cliente) {

			Cliente clienteInsertado = this.clienteJDBCDAO.insert(cliente);
			if(clienteInsertado == null) {
				throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
						"Entity not created.");
			}

			return clienteInsertado;
		}

		@GetMapping("/count")
		public Integer count() {
			return this.clienteJDBCDAO.count();
		}

		@GetMapping("")
		public List<Cliente> findAll() {
			return this.clienteJDBCDAO.findAll();
		}

		@GetMapping("/{id}")
		public Cliente findById(@PathVariable int id) {
			Cliente cliente = this.clienteJDBCDAO.findById(id);
			if(cliente == null) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND,
						"Entity not found.");
			}
			return cliente;
		}

		@PutMapping("/{id}")
		@ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity updated" )
		public void update(@PathVariable int id, @RequestBody Cliente cliente) {

			boolean updated = this.clienteJDBCDAO.update(cliente);
			if(!updated) {
				throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
						"Entity not updated.");
			}
		}

		@DeleteMapping("/{id}")
		@ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity deleted" )
		public void delete(@PathVariable int id, @RequestBody Cliente cliente) {

			boolean deleted = this.clienteJDBCDAO.delete(cliente);
			if(!deleted) {
				throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
						"Entity not deleted.");
			}
		}
}
