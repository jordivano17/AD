package es.cipfpbatoi.ud02a01.repository.jdbc.facturas;

import es.cipfpbatoi.ud02a01.model.*;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

class FacturasWithAllRowMapper implements RowMapper<Facturas> {

    private Map<Integer, Facturas> facturasMap = new HashMap<>();

    @Override
    public Facturas mapRow(ResultSet rs, int rowNum) throws SQLException {

        Integer idFra = rs.getInt("facturas.id");
        Facturas factu = this.facturasMap.get(idFra);

        if(factu==null){

            factu = new Facturas();

        factu.setId(rs.getInt("facturas.id"));
        factu.setFecha(rs.getDate("facturas.fecha"));
        factu.setCliente(rs.getInt("facturas.cliente"));
        factu.setVendedor(rs.getInt("factuas.vendedor"));
        factu.setFormapago(rs.getString("facturas.formapago"));

        Cliente cliente = new Cliente();
        cliente.setId(rs.getInt("clientes.id"));
        cliente.setDireccion(rs.getString("clientes.direccion"));
        cliente.setNombre(rs.getString("clientes.nombre"));

        factu.setInfoCliente(cliente);

        Vendedor vendedor = new Vendedor();
        vendedor.setId(rs.getInt("vendedores.id"));
        vendedor.setFechaIngreso(rs.getDate("vendedores.fecha_ingreso"));
        vendedor.setNombre(rs.getString("vendedores.nombre"));
        vendedor.setSalario(rs.getDouble("vendedores.salario"));

        factu.setInfoVendedor(vendedor);

        this.facturasMap.put(idFra, factu);
    }

        LineaFacturas linea = new LineaFacturas();

        linea.setLinea(rs.getInt("lineas_factura.linea"));
        linea.setFactura(rs.getInt("Lineas_factura.factura"));
        linea.setIdArticulo(rs.getInt("lineas_factura.articulo"));
        linea.setCantidad(rs.getInt("lineas_factura.cantidad"));
        linea.setImporte(rs.getInt("lineas_factura.importe"));

        Articulos articulo = new Articulos();

        articulo.setId(rs.getInt("articulos.id"));
        articulo.setCodigo(rs.getString("articulos.codigo"));
        articulo.setIdGrupo(rs.getInt("articulos.grupo"));
        articulo.setNombre(rs.getString("articulos.nombre"));
        articulo.setPrecio(rs.getDouble("articulos.precio"));

        linea.setArticulo(articulo);

        factu.addLineaFacturas(linea);

        return factu;
    }
}