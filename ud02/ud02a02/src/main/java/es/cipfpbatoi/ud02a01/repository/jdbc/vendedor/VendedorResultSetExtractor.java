package es.cipfpbatoi.ud02a01.repository.jdbc.vendedor;


import es.cipfpbatoi.ud02a01.model.Vendedor;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VendedorResultSetExtractor implements ResultSetExtractor<Vendedor> {
    @Override
    public Vendedor extractData(ResultSet rs) throws SQLException, DataAccessException {

        if(!rs.next()) {
            return null;
        }

        Vendedor vendedor = new Vendedor();

        vendedor.setId(rs.getInt("id"));
        vendedor.setNombre(rs.getString("nombre"));
        vendedor.setFechaIngreso(rs.getDate("fecha_ingreso"));
        vendedor.setSalario(rs.getDouble("salario"));

        return vendedor;
    }
}
