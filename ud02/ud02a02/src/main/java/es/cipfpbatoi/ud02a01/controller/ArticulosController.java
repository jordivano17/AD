package es.cipfpbatoi.ud02a01.controller;


import es.cipfpbatoi.ud02a01.model.Articulos;
import es.cipfpbatoi.ud02a01.model.Cliente;
import es.cipfpbatoi.ud02a01.repository.jdbc.articulos.ArticulosJDBCDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/articulos")
public class ArticulosController {
    @Autowired
    ArticulosJDBCDAO articulosJDBCDAO;
    @PostMapping("")
    @ResponseStatus(code= HttpStatus.CREATED)
    public Articulos create(@RequestBody Articulos articulo) {

        Articulos articuloinsertado = this.articulosJDBCDAO.insert(articulo);
        if(articuloinsertado == null) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not created.");
        }

        return articuloinsertado;
    }

    @GetMapping("/count")
    public Integer count() {
        return this.articulosJDBCDAO.count();
    }

    @GetMapping("")
    public List<Articulos> findAll() {
        return this.articulosJDBCDAO.findAll();
    }



    @PutMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity updated" )
    public void update(@PathVariable int id, @RequestBody Articulos articulo) {

        boolean updated = this.articulosJDBCDAO.update(articulo);
        if(!updated) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not updated.");
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code=HttpStatus.NO_CONTENT, reason="Entity deleted" )
    public void delete(@PathVariable int id, @RequestBody Articulos articulo) {

        boolean deleted = this.articulosJDBCDAO.delete(articulo);
        if(!deleted) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,
                    "Entity not deleted.");
        }
    }

    @GetMapping(value="", params = "fileFormat")
    public ResponseEntity<Resource> findAll(@RequestParam(required = false) String fileFormat) {
        return null;
    }

    @GetMapping(value="/{id}")
    public Articulos findById(@PathVariable int id,
                             @RequestParam(required = false,
                                     defaultValue = "false")
                                     Boolean withGroup) {

        Articulos articulo = null;

        if(!withGroup) {
            articulo = this.articulosJDBCDAO.findByIdWithGrupo(id);
        } else {
            articulo = this.articulosJDBCDAO.findById(id);
        }

        if(articulo == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Entity not found.");
        }
        return articulo;
    }

}
